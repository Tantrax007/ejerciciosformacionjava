package com.bosonit.formacion.demorestcontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRestControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoRestControllerApplication.class, args);
    }

}
