package com.bosonit.formacion.demorestcontroller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class MiControlador {

    @GetMapping(value = "/api/hola")
    public String hola(@RequestHeader MultiValueMap<String, String> mapa) {
        return mapa.toString();
    }

}
