package com.bosonit.formacion.spring.ejerciciosa2.infrastructure;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;

@RestController
@RequestMapping(value="/file")
public class FileResource {

    //Define the location for the files
    public static final String DIRECTORY = System.getProperty("user.home") + "/Documents/uploads"; //Path of the user + the path of my server

    //Endpoint to upload files
    @PostMapping(value="/upload")
    public ResponseEntity<List<String>> uploadFiles(@RequestParam("files") List<MultipartFile> multipartFiles){ //This method is going to return a list of all the name files that the user upload
        List<String> fileNames = new ArrayList<>();

        multipartFiles.forEach(file -> {
            String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename())); //cleanPath clean the string out of special characters

            Path filePath = get(DIRECTORY, filename).toAbsolutePath().normalize();

            try {

                copy(file.getInputStream(), filePath, REPLACE_EXISTING); //In case that the file exists
                fileNames.add(filename);

            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        return ResponseEntity.ok().body(fileNames); //Return an 200 OK response and in the body the list
    }

    //Define a method to download files
    @GetMapping(value="/download/{filename}")
    public ResponseEntity<Resource> downloadFiles(@PathVariable(value="filename") String filename) throws IOException {
        Path filePath = get(DIRECTORY).toAbsolutePath().normalize().resolve(filename); //Save the absolute path of the file that the user wants to download

        if(!Files.exists(filePath)){ //If the file dosn't exists
            throw new FileNotFoundException(filename + "this file was not fond on the server");
        }

        Resource resource = new UrlResource(filePath.toUri()); //Create a resource with the filePath

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("File-Name", filename); //Add to the header the name of the file
        httpHeaders.add(CONTENT_DISPOSITION, "attachment;File-Name=" + resource.getFilename()); //resource.getFilename() -> Te same of -> filename

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(Files.probeContentType(filePath))).headers(httpHeaders).body(resource);
    }



}
