package com.bosonit.formacion.spring.ejerciciosa2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioSa2Application {

    public static void main(String[] args) {
        SpringApplication.run(EjercicioSa2Application.class, args);
    }

}
