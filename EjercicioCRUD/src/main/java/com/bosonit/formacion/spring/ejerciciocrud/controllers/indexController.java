package com.bosonit.formacion.spring.ejerciciocrud.controllers;

import com.bosonit.formacion.spring.ejerciciocrud.domain.IPersona;
import com.bosonit.formacion.spring.ejerciciocrud.domain.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/URL")
public class indexController {

    @Autowired
    private IPersona person;

//    public indexController(){ Null pointer exception porque todavia no se ha creado el objeto de la clase, funcionaria si utilizasemos el @PostConstructor
//        person.getPersona();
//    }

    @GetMapping("/persona/{id}") //Obtener Persona por id
    public Persona getPersonaPorId(@PathVariable int id){
        Map<Integer, Persona> personas = person.getPersona();

        return personas.get(id);
    }

    @GetMapping("/persona/nombre/{nombre}") //Obtener Persona por nombre
    public List<Persona> getPersonaPorNombre(@PathVariable String nombre){
        Map<Integer, Persona> personas = person.getPersona();

        List<Persona> encontrados = new ArrayList<Persona>();
        //Cogemos los valores del mapa, los convertimos a un stream, filtramos por aquella persona cuyo nombre sea igual al nombre incluido y lo pasamos a una lista
        encontrados = personas.values().stream().filter(per -> per.getNombre().equals(nombre)).collect(Collectors.toList());

        return encontrados;
    }

    @PostMapping("/persona")
    public void addPersona(@RequestBody Persona persona){
        person.addPersona(persona);
    }

    @PutMapping("/persona/{id}")
    public void putPersona(@PathVariable int id, @RequestBody Persona persona){
        person.updatePersona(id, persona);
    }

    @DeleteMapping("/persona/{id}")
    public void eliminarPersona(@PathVariable int id){
        person.deletePersona(id);
    }
}