package com.bosonit.formacion.spring.ejerciciocrud.domain;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PersonaService implements IPersona {
    private static int id;
    Persona persona = new Persona();
    Map<Integer, Persona> personas = new HashMap<>();


    @Override
    public void addPersona(Persona persona) {
        id += 1;
        this.persona.setNombre(persona.getNombre());
        this.persona.setEdad(persona.getEdad());
        this.persona.setPoblacion(persona.getPoblacion());
        personas.put(id, persona);
    }

    @Override
    public Map<Integer, Persona> getPersona() {
        return personas;
    }

    @Override
    public void updatePersona(int id, Persona persona) { //Todo Podria ser un boolean para retornar si se ha podido modificar o no
        Persona parAEditar = new Persona(); //Creamos una persona donde vamos a almacenar la que vamos a editar

        if (personas.containsKey(id)){ //Si el id que obtenemos existe
            parAEditar = personas.get(id); //Obtenemos la persona

            if (persona.getNombre() != null){
                persona.setNombre(persona.getNombre()); //Update del nombre de la persona
            }

            if (persona.getPoblacion() != null){
                persona.setPoblacion(persona.getPoblacion()); //Update de la poblacion de la persona
            }

            if (persona.getEdad() != 0){
                persona.setEdad(persona.getEdad()); //Update de la edad de la persona
            }

            personas.put(id, parAEditar); //Actualizamos en el HashMap
        }
    }

    @Override
    public void deletePersona(int id) { //Todo podria ser un boolean para retornar si se ha podido eliminar la persona o no
        if (personas.containsKey(id)) { //Si la persona con ese Id existe
            personas.remove(id);
        }
    }
}