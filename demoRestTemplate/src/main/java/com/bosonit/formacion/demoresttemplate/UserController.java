package com.bosonit.formacion.demoresttemplate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @GetMapping(value="/getUsuario")
    public Customer getUsuario() {
        Customer customer = new Customer();
        customer.setName("Alberto");
        customer.setAddress("Calle Pdte. Calvo Sotelo 30");

        return customer;
    }
}
