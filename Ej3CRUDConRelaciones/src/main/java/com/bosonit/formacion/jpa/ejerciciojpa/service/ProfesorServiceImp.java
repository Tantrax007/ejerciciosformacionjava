package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpProfesorDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutProfesorDTOComplex;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutProfesorDTOSimple;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Estudiante_Asignatura;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Profesor;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Student;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UnProcesableException;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UserNotFoundException;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.PersonaRepository;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.ProfesorRepository;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProfesorServiceImp implements IProfesor {
    @Autowired
    private ProfesorRepository profesorRepository;

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public OutProfesorDTOSimple getProfesor(String id, String outputType) throws Exception {
        if(profesorRepository.findById(id).isPresent()){
            if(outputType.equalsIgnoreCase("simple")){
                return new OutProfesorDTOSimple(profesorRepository.findById(id).get());
            }
            else if(outputType.equalsIgnoreCase("full")){
                return new OutProfesorDTOComplex(profesorRepository.findById(id).get());
            }
            else{
                throw new UnProcesableException("Selecciona una opcion correcta");
            }
        }
        else {
            throw new UserNotFoundException("No existe ningun profesor con ese ID");
        }
    }

    @Override
    public List<OutProfesorDTOSimple> getProfesores(String outputType) throws Exception {
        List<OutProfesorDTOSimple> profesores = new ArrayList<>();

        if(outputType.equalsIgnoreCase("simple")){
            profesorRepository.findAll().forEach((profesor) -> profesores.add(new OutProfesorDTOSimple(profesor)));
            return profesores;
        }
        else if(outputType.equalsIgnoreCase("full")){
            profesorRepository.findAll().forEach((profesor) -> profesores.add(new OutProfesorDTOComplex(profesor)));
            return profesores;
        }
        else{
            throw new UnProcesableException("Selecciona una opcion correcta");
        }
    }

    @Override
    public void addProfesor(InpProfesorDTO profesorEntrada) {
        if(personaRepository.findById(profesorEntrada.getPersona_id()).isPresent()){
            profesorRepository.save(transProfesorIn(profesorEntrada));
        }
        else{
            throw new UserNotFoundException("No existe ninguna persona con ese ID");
        }
    }

    @Override
    public OutProfesorDTOSimple modifyProfesor(String id, InpProfesorDTO profesorEntrada) {
        if(profesorRepository.findById(id).isPresent()){
            Profesor profesorObtenido = profesorRepository.findById(id).get();
            modificarProfesor(profesorObtenido, profesorEntrada);
            profesorRepository.save(profesorObtenido);
            return new OutProfesorDTOSimple(profesorObtenido);
        }
        else{
            throw new UserNotFoundException("No existe profesor con esa ID");
        }
    }

    @Override
    public void deleteProfesor(String id) throws UserNotFoundException {
        if(profesorRepository.findById(id).isPresent()){
            profesorRepository.deleteById(id);
        }
        else{
            throw new UserNotFoundException("El profesor que quiere eliminar no existe");
        }
    }

    private Profesor transProfesorIn(InpProfesorDTO profesorEntrada){
        Profesor profesor = new Profesor();
        List<Student> estudiantes = new ArrayList<>();

        profesor.setPersona(personaRepository.findById(profesorEntrada.getPersona_id()).get()); //No hace falta volver a tirar excepcion porque ya se hace en el metodo addProfesor();
        profesor.setBranch(profesorEntrada.getBranch());
        profesor.setComents(profesorEntrada.getComents());

//        profesorEntrada.getEstudiantes().forEach((id_estudiante) -> {
//            if(studentRepository.findById(id_estudiante).isPresent()){
//                estudiantes.add(studentRepository.findById(id_estudiante).get());
//            }
//            else{
//                throw new UserNotFoundException("No existe alumno con el id: " + id_estudiante);
//            }
//        });

        profesorEntrada.getEstudiantes().forEach(
                (id_estudiante) -> studentRepository.findById(id_estudiante).orElseThrow(() -> new UserNotFoundException("No existe alumno con id: " + id_estudiante))
        );

        return profesor;
    }

    private void modificarProfesor(Profesor profesorObtenido, InpProfesorDTO profesorEntrada){
        List<Student> estudiantes = new ArrayList<>();

        if(profesorEntrada.getComents() != null){
            profesorObtenido.setComents(profesorEntrada.getComents());
        }

        if(profesorEntrada.getBranch() != null){
            profesorObtenido.setBranch(profesorEntrada.getBranch());
        }

        if(profesorEntrada.getEstudiantes().size() > 0){
            profesorEntrada.getEstudiantes().forEach(
                    (id_estudiante) -> estudiantes.add(studentRepository.findById(id_estudiante).orElseThrow(() -> new UserNotFoundException("No existe alumno con id: " + id_estudiante)))
            );
            profesorObtenido.setEstudiantes(estudiantes);
        }
    }
}
