package com.bosonit.formacion.jpa.ejerciciojpa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@RestControllerAdvice
public class RespuestaMalaPeticion {
    @ExceptionHandler(HttpClientErrorException.BadRequest.class)
    public final ResponseEntity<CustomError> handleBadRequestException(HttpClientErrorException.BadRequest br, WebRequest request){
        CustomError exceptionResponse = new CustomError(new Date(), br.getMessage(), request.getDescription(false), HttpStatus.BAD_REQUEST.getReasonPhrase());
        return new ResponseEntity<CustomError>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
}
