package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UserNotFoundException;

import java.util.List;

public interface IPersona {
    public void setPersonaDTO(InpPersonaDTO personaEntrada) throws UserNotFoundException;
    public OutPersonaDTO getPersonaDTO(int id) throws UserNotFoundException;
    public List<OutPersonaDTO> getAllPersonas();
    public void removePersona(int id) throws UserNotFoundException;
}
