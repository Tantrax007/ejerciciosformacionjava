package com.bosonit.formacion.jpa.ejerciciojpa.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="estudiante_asignaturas")
@Getter
@Setter
public class Estudiante_Asignatura {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id_asignatura;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "estudiantes_estudios",
            joinColumns = @JoinColumn(name = "id_asignatura"),
            inverseJoinColumns = @JoinColumn(name = "id_student")
    )
    private List<Student> studients = new ArrayList<>();

    @Column(name = "asignatura")
    private String asignatura;

    @Column(name = "coments")
    private String coments;

    @Column(name = "initial_date")
    private Date initial_date;

    @Column(name = "finish_date")
    private Date finish_date;
}
