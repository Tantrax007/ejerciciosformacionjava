package com.bosonit.formacion.jpa.ejerciciojpa.controller;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpStudentDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutStudentDTOSimple;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UserNotFoundException;
import com.bosonit.formacion.jpa.ejerciciojpa.service.IStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/student")
public class StudentController {

    @Autowired
    private IStudent iStudent;

    @GetMapping(value="/getEstudiante/{id}")
    public OutStudentDTOSimple getEstudiante(@PathVariable String id, @RequestParam(value = "outputType", defaultValue = "simple") String outputType) throws Exception {
        return iStudent.getStudent(id, outputType);
    }

    @GetMapping(value="/getEstudiantes")
    public List<OutStudentDTOSimple> getEstudiantes(@RequestParam(value = "outputType", defaultValue = "simple") String outputType) throws Exception{
        return iStudent.getStudents(outputType);
    }

    @PostMapping(value="/addEstudiante")
    public void addEstudiante(@RequestBody InpStudentDTO estudianteEntrada) throws UserNotFoundException {
        iStudent.addStudent(estudianteEntrada);
    }

    @PutMapping(value="/modEstudiante/{id}")
    public OutStudentDTOSimple modificarEstudiante(@PathVariable String id, @RequestBody InpStudentDTO estudianteEntrada){
        return iStudent.modifyStudent(id, estudianteEntrada);
    }

    @DeleteMapping(value="/delEstudiante/{id}")
    public void eliminarEstudiante(@PathVariable String id){
        iStudent.deleteStudent(id);
    }
}
