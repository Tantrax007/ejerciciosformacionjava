package com.bosonit.formacion.jpa.ejerciciojpa.controller;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpProfesorDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutProfesorDTOSimple;

import com.bosonit.formacion.jpa.ejerciciojpa.service.IProfesor;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value="/profesor")
public class ProfesorController {

    @Autowired
    private IProfesor iProfesor;

    @GetMapping(value="/getProfesor/{id}")
    public OutProfesorDTOSimple getProfesor(@PathVariable String id, @RequestParam(value="outputType", defaultValue = "simple") String outputType) throws Exception{
        return iProfesor.getProfesor(id, outputType);
    }

    @PostMapping(value="/addProfesor")
    public void addProfesor(@RequestBody InpProfesorDTO profesorEntrada){
        iProfesor.addProfesor(profesorEntrada);
    }

    @PutMapping(value="/modProfesor/{id}")
    public OutProfesorDTOSimple modifyProfesor(@PathVariable String id, @RequestBody InpProfesorDTO profesorEntrada){
        return iProfesor.modifyProfesor(id, profesorEntrada);
    }
}
