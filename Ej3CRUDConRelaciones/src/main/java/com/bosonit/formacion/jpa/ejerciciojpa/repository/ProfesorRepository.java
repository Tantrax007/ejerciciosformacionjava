package com.bosonit.formacion.jpa.ejerciciojpa.repository;

import com.bosonit.formacion.jpa.ejerciciojpa.entity.Persona;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Profesor;
import org.springframework.data.repository.CrudRepository;

public interface ProfesorRepository extends CrudRepository<Profesor, String> {

}
