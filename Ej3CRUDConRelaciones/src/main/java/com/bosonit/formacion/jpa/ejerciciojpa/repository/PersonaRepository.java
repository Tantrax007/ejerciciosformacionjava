package com.bosonit.formacion.jpa.ejerciciojpa.repository;

import com.bosonit.formacion.jpa.ejerciciojpa.entity.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Integer> {
}
