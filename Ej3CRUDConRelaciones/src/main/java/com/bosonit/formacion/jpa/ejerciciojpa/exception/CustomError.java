package com.bosonit.formacion.jpa.ejerciciojpa.exception;

import java.util.Date;

public class CustomError { //Esta clase conforma el JSON que vamos a devolver en el error 404 not found
    private Date Fecha;
    private String mensaje;
    private String CodigoDeError;

    public CustomError(Date timestamp, String mensaje, String detalles, String httpCodeMessage) {
        this.Fecha = timestamp;
        this.mensaje = mensaje;
        this.CodigoDeError = httpCodeMessage;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        this.Fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCodigoDeError() {
        return CodigoDeError;
    }

    public void setCodigoDeError(String codigoDeError) {
        this.CodigoDeError = codigoDeError;
    }
}
