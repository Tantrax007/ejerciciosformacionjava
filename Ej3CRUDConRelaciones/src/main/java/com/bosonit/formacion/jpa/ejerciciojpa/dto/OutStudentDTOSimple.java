package com.bosonit.formacion.jpa.ejerciciojpa.dto;


import com.bosonit.formacion.jpa.ejerciciojpa.entity.Student;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;


@Component
@Getter
@Setter
@NoArgsConstructor
public class OutStudentDTOSimple {
    private String id_student;
    private int id_persona;
    private int num_hours_week;
    private String coments;
    private String branch;

    public OutStudentDTOSimple(Student estudiante){
        setId_student(estudiante.getId_student());
        setId_persona(estudiante.getPersona().getId_persona());
        setNum_hours_week(estudiante.getNum_hours_week());
        setComents(estudiante.getComents());
        setBranch(estudiante.getBranch());
    }
}
