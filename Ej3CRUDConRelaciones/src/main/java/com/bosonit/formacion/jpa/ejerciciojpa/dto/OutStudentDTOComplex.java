package com.bosonit.formacion.jpa.ejerciciojpa.dto;


import com.bosonit.formacion.jpa.ejerciciojpa.entity.Estudiante_Asignatura;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Student;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;


@Component
@Getter
@Setter
@NoArgsConstructor
public class OutStudentDTOComplex extends OutStudentDTOSimple{
    @NotNull
    private OutPersonaDTO outPersonaDTO;
    private List<Estudiante_Asignatura> estudios;

    public OutStudentDTOComplex(Student estudiante){
        super(estudiante);
        setOutPersonaDTO(new OutPersonaDTO(estudiante.getPersona()));
        setEstudios(estudiante.getEstudios());
    }
}
