package com.bosonit.formacion.jpa.ejerciciojpa.entity;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpPersonaDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "personas")
@Getter
@Setter
@NoArgsConstructor
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_persona;

    @OneToOne(mappedBy = "persona")
    private Student estudiante;

    @OneToOne(mappedBy = "persona")
    private Profesor profesor;

    @NotNull
    private String usuario;

    @NotNull
    private String password;

    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private boolean active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;
}
