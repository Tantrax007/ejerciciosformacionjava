package com.bosonit.formacion.jpa.ejerciciojpa.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Component
public class InpEstudiosDTO {
    private List<Integer> studients = new ArrayList<>();
    private String asignatura;
    private String coments;
    private Date initial_date;
    private Date finish_date;
}
