package com.bosonit.formacion.jpa.ejerciciojpa.dto;

import com.bosonit.formacion.jpa.ejerciciojpa.entity.Persona;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Student;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class InpProfesorDTO {
    private int persona_id; // Va a ser un objeto persona ya que es algo integro del propio profesor
    private String coments;
    private String branch;
    List<String> estudiantes = new ArrayList<>();
}
