package com.bosonit.formacion.jpa.ejerciciojpa.controller;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UserNotFoundException;
import com.bosonit.formacion.jpa.ejerciciojpa.service.IPersona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/persona")
public class PersonaController {

    @Autowired
    private IPersona iPersona;


    @GetMapping(value="/getPersona/porId/{id}")
    public OutPersonaDTO getPersona(@PathVariable int id) throws UserNotFoundException{
        return iPersona.getPersonaDTO(id);
    }

    @GetMapping(value="/getPersona/all")
    public List<OutPersonaDTO> getPersonas() {
        return iPersona.getAllPersonas();
    }

    @CrossOrigin(origins = "https://cdpn.io") //Esto se ve en el navegador en la request del navegador al servidor, en este caso este es el dominio de codePen desde el que se realiza la peticion
    @PostMapping(value="/addperson")
    public void setPersona(@RequestBody InpPersonaDTO personaEntrada) throws UserNotFoundException {
        iPersona.setPersonaDTO(personaEntrada);
    }

    @DeleteMapping(value="/delPersona/{id}")
    public void removePersona(@PathVariable int id) throws UserNotFoundException{
         iPersona.removePersona(id);
    }
}
