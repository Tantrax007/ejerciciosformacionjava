package com.bosonit.formacion.jpa.ejerciciojpa.dto;

import com.bosonit.formacion.jpa.ejerciciojpa.entity.Persona;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Profesor;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class InpStudentDTO {
    @NotNull
    private int persona_id;

    @NotNull
    private String profesor_id;

    private int num_hours_week;
    private String coments;
    private String branch;
    private List<String> estudios = new ArrayList<>();
}