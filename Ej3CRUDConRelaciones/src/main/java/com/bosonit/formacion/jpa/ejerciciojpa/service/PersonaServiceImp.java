package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Persona;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Profesor;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UnProcesableException;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UserNotFoundException;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.PersonaRepository;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.ProfesorRepository;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonaServiceImp implements IPersona{

    @Autowired
    PersonaRepository personaRepository;

    @Autowired
    ProfesorRepository profesorRepository;

    @Override
    public void setPersonaDTO(@Valid InpPersonaDTO personaEntrada) throws UserNotFoundException {
        if(validarInpPersona(personaEntrada)){
            Persona per = new Persona();
            per.setUsuario(personaEntrada.getUsuario());
            per.setPassword(personaEntrada.getPassword());
            per.setName(personaEntrada.getName());
            per.setSurname(personaEntrada.getSurname());
            per.setCompany_email(personaEntrada.getCompany_email());
            per.setPersonal_email(personaEntrada.getPersonal_email());
            per.setCity(personaEntrada.getCity());
            per.setActive(personaEntrada.getActive().equalsIgnoreCase("true"));
            per.setCreated_date(personaEntrada.getCreated_date());
            per.setImagen_url(personaEntrada.getImagen_url());
            per.setTermination_date(personaEntrada.getTermination_date());
            personaRepository.save(per);
        }
    }

    @Override
    public OutPersonaDTO getPersonaDTO(int id) throws UserNotFoundException{
        Persona per = personaRepository.findById(id).orElseThrow(() -> new UserNotFoundException("No hemos encontrado el usuario que busca"));

//        Esto soluciona el problema de tener que manualmente pasar la entidad persona en un outputPersona
//        BeanUtils.copyProperties(per, outputPersona); Ya no es necesario porque lo mando al constructor de OutPersonaDTO

//        outputPersona.setId_persona(per.getId_persona());
//        outputPersona.setUsuario(per.getUsuario());
//        outputPersona.setName(per.getName());
//        outputPersona.setSurname(per.getSurname());
//        outputPersona.setCompany_email(per.getCompany_email());
//        outputPersona.setPersonal_email(per.getPersonal_email());
//        outputPersona.setCity(per.getCity());
//        outputPersona.setActive(per.isActive());
//        outputPersona.setCreated_date(per.getCreated_date());
//        outputPersona.setImagen_url(per.getImagen_url());
//        outputPersona.setTermination_date(per.getTermination_date());

        return new OutPersonaDTO(per);
    }

    @Override
    public List<OutPersonaDTO> getAllPersonas(){
        List<OutPersonaDTO> personas = new ArrayList<>();

        personaRepository.findAll().forEach(per -> {
            personas.add(new OutPersonaDTO(per));
        }); //Guardamos todos los resultados

        return personas;
    }

    @Override
    public void removePersona(int id){
        if(personaRepository.findById(id).isPresent()){
            Persona personaAEliminar = personaRepository.findById(id).get();
            Profesor profesor = personaAEliminar.getProfesor();
            profesor.setPersona(null);
            personaRepository.deleteById(id);
        }
        else{
            throw new UserNotFoundException("La persona que quieres eliminar no existe");
        }
    }

    private boolean validarInpPersona(InpPersonaDTO personaDTO) throws UserNotFoundException {
        if(personaDTO.getUsuario() != null){
            if((personaDTO.getUsuario().length() >= 6) && (personaDTO.getUsuario().length() <= 10)){
                if(personaDTO.getPassword() != null){
                    if(personaDTO.getCompany_email() != null){
                        if(personaDTO.getPersonal_email() != null){
                            if(personaDTO.getCity() != null){
                                if(personaDTO.getActive() != null){
                                    if((personaDTO.getActive().equalsIgnoreCase("true")) || (personaDTO.getActive().equalsIgnoreCase("false"))){
                                        if(personaDTO.getCreated_date() != null){
                                            if(personaDTO.getImagen_url() != null){
                                                if(personaDTO.getTermination_date() != null){
                                                    return true;
                                                }
                                                else{
                                                    throw new UnProcesableException("La fecha de finalizacion no puede ser nula");
                                                }
                                            }
                                            else{
                                                throw new UnProcesableException("La foto no puede ser nula");
                                            }
                                        }
                                        else{
                                            throw new UnProcesableException("La fecha de creacion no puede ser nula");
                                        }
                                    }
                                    else{
                                        throw new UnProcesableException("El estado tiene que ser true o false");
                                    }
                                }
                                else{
                                    throw new UnProcesableException("El usuario tiene que tener estado de activo");
                                }
                            }
                            else{
                                throw new UnProcesableException("La ciudad no puede ser nula");
                            }
                        }
                        else{
                            throw new UnProcesableException("El email personal no puede ser nulo");
                        }
                    }
                    else{
                        throw new UnProcesableException("El email de la compañia no puede ser nulo");
                    }
                }
                else{
                    throw new UnProcesableException("La contraseña no puede ser nula");
                }
            }
            else{
                throw new UnProcesableException("El nombre del usaurio tiene que ser mayor o igual a 6 y menor o igual a 10 caracteres");
            }
        }
        else{
            throw new UnProcesableException("El nombre del usaurio no puede ser nulo");
        }
    }
}
