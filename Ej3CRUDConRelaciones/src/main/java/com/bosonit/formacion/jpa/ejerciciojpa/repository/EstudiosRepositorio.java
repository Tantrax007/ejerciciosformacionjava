package com.bosonit.formacion.jpa.ejerciciojpa.repository;

import com.bosonit.formacion.jpa.ejerciciojpa.entity.Estudiante_Asignatura;
import org.springframework.data.repository.CrudRepository;

public interface EstudiosRepositorio extends CrudRepository<Estudiante_Asignatura, String> {
}
