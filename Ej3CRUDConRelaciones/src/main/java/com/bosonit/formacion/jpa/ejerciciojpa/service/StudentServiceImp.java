package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpStudentDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutStudentDTOComplex;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutStudentDTOSimple;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Estudiante_Asignatura;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Student;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UnProcesableException;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UserNotFoundException;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.EstudiosRepositorio;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.PersonaRepository;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.ProfesorRepository;
import com.bosonit.formacion.jpa.ejerciciojpa.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImp implements IStudent {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    PersonaRepository personaRepository;

    @Autowired
    ProfesorRepository profesorRepository;

    @Autowired
    EstudiosRepositorio estudiosRepositorio;

    @Override
    public void addStudent(InpStudentDTO studentEntrada) throws UserNotFoundException {
        Student estudiante = new Student();
        List<Estudiante_Asignatura> estudios = new ArrayList<>();

        estudiante.setPersona(personaRepository.findById(studentEntrada.getPersona_id()).orElseThrow(() -> new UserNotFoundException("No existe una persona con ese ID")));

        estudiante.setProfesor(profesorRepository.findById(studentEntrada.getProfesor_id()).orElseThrow(() -> new UserNotFoundException("No existe un profesor con ese ID")));

        estudiante.setNum_hours_week(studentEntrada.getNum_hours_week());
        estudiante.setComents(studentEntrada.getComents());
        estudiante.setBranch(studentEntrada.getBranch());
        studentEntrada.getEstudios().forEach((id_estudio) -> estudios.add(estudiosRepositorio.findById(id_estudio).orElse(null))); //Si algun estudion no existe lo dejamos nulo
        estudiante.setEstudios(estudios);

        studentRepository.save(estudiante);
    }

//    private Persona rellenarPersona(Persona personaARellenar) throws Exception{
//        Persona personaDelAlumno = new Persona();
//
//        personaDelAlumno.setUsuario(personaARellenar.getUsuario());
//        personaDelAlumno.setPassword(personaARellenar.getPassword());
//        personaDelAlumno.setCompany_email(personaARellenar.getCompany_email());
//        personaDelAlumno.setPersonal_email(personaARellenar.getPersonal_email());
//        personaDelAlumno.setCity(personaARellenar.getCity());
//        personaDelAlumno.setActive(personaARellenar.isActive());
//        personaDelAlumno.setCreated_date(personaARellenar.getCreated_date());
//        personaDelAlumno.setImagen_url(personaARellenar.getImagen_url());
//        personaDelAlumno.setTermination_date(personaARellenar.getTermination_date());
//
//        personaRepository.save(personaDelAlumno); //Una vez rellenada la persona la guardamos
//
//        return personaDelAlumno;
//    }

    @Override
    public OutStudentDTOSimple getStudent(String id, String outputType) throws UserNotFoundException {

        if(studentRepository.findById(id).isPresent()){ //Comprobar si existe el estudiante
            if(outputType.equalsIgnoreCase("full")){ //Si es de tipo full
                return new OutStudentDTOComplex(studentRepository.findById(id).get());
            }
            else if(outputType.equalsIgnoreCase("simple")){
                return new OutStudentDTOSimple(studentRepository.findById(id).get());
            }
            else{
                throw new UnProcesableException("Operacion no valida");
            }
        }
        else{
            throw new UserNotFoundException("No se ha encontrado ningun estudiante con ese ID");
        }
    }

    public List<OutStudentDTOSimple> getStudents(String outputType) throws Exception{
        List<OutStudentDTOSimple> personasSimple = new ArrayList<>();

        if(outputType.equalsIgnoreCase("simple")){
            studentRepository.findAll().forEach((persona) -> personasSimple.add(new OutStudentDTOSimple(persona)));

            if(personasSimple.size() > 0){
                return personasSimple;
            }
            else{
                throw new UserNotFoundException("No hay estudiantes para mostrar");
            }
        }
        else if(outputType.equalsIgnoreCase("full")){
            studentRepository.findAll().forEach((persona) -> personasSimple.add(new OutStudentDTOComplex(persona)));

            if(personasSimple.size() > 0){
                return personasSimple;
            }
            else{
                throw new UserNotFoundException("No hay estudiantes para mostrar");
            }
        }
        else{
            throw new UnProcesableException("Operacion no valida");
        }
    }

//    @Override
//    public OutStudentDTOComplex getStudentCompleto(String id) throws UserNotFoundException {
//        if(studentRepository.findById(id).isPresent()){
//            return StudentToOutStudentComplex(studentRepository.findById(id).get());
//        }
//        else{
//            throw new UserNotFoundException("El estudiante que estas buscando no existe");
//        }
//    }

    public OutStudentDTOSimple modifyStudent(String id, InpStudentDTO estudianteEntrada) throws UserNotFoundException{
        Student estudianteObtenido = studentRepository.findById(id).orElseThrow(() -> new UserNotFoundException("El alumno que quieres modificar no existe"));
        studentRepository.save(estudianteObtenido);
        return new OutStudentDTOSimple(estudianteObtenido);
    }

    @Override
    public void deleteStudent(String id) {
        if(studentRepository.findById(id).isPresent()){
            studentRepository.deleteById(id);
        }
        else{
            throw new UserNotFoundException("El alumno que quieres eliminar no existe");
        }
    }

    private void actualizarEstudiante(Student estudianteObtenido, InpStudentDTO estudianteEntrada){
        List<Estudiante_Asignatura> estudios = new ArrayList<>();

        if(estudianteEntrada.getProfesor_id() != null){
            estudianteObtenido.setProfesor(profesorRepository.findById(estudianteEntrada.getProfesor_id()).orElseThrow(() -> new UserNotFoundException("El profesor digitado no existe")));
        }

        if(estudianteEntrada.getBranch() != null){
            estudianteObtenido.setBranch(estudianteEntrada.getBranch());
        }


        if(estudianteEntrada.getComents() != null){
            estudianteObtenido.setComents(estudianteEntrada.getComents());
        }

        if(estudianteEntrada.getNum_hours_week() > 0){
            estudianteObtenido.setNum_hours_week(estudianteEntrada.getNum_hours_week());
        }

        if(estudianteEntrada.getEstudios().size() > 0) {
            estudianteEntrada.getEstudios().forEach((id_estudio) -> estudios.add(estudiosRepositorio.findById(id_estudio).orElseThrow(() -> new UserNotFoundException("El estudio digitado no existe"))));
            estudianteObtenido.setEstudios(estudios);
        }
    }
}
