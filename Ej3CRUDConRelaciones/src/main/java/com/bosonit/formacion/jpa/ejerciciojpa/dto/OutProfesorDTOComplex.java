package com.bosonit.formacion.jpa.ejerciciojpa.dto;

import com.bosonit.formacion.jpa.ejerciciojpa.entity.Profesor;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Student;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@Getter
@Setter
@NoArgsConstructor
public class OutProfesorDTOComplex extends OutProfesorDTOSimple{
    private OutPersonaDTO personaDTO;
    private List<Student> estudiantes;

    public OutProfesorDTOComplex(Profesor profesor){
        super(profesor);
        setPersonaDTO(new OutPersonaDTO(profesor.getPersona()));
        setEstudiantes(profesor.getEstudiantes());
    }
}
