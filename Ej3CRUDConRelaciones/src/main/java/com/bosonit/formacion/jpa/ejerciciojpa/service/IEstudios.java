package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpEstudiosDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutEstudiosDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.entity.Estudiante_Asignatura;

import java.util.List;

public interface IEstudios {
    public void setEstudios(InpEstudiosDTO estudiosEntrada) throws Exception;
    public OutEstudiosDTO getEstudio(int id) throws Exception;
    public List<Estudiante_Asignatura> getEstudios();
}
