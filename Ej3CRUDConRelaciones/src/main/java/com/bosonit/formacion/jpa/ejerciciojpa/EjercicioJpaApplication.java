package com.bosonit.formacion.jpa.ejerciciojpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(EjercicioJpaApplication.class, args);
    }

}
