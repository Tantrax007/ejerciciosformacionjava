package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpStudentDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutStudentDTOSimple;
import com.bosonit.formacion.jpa.ejerciciojpa.exception.UserNotFoundException;

import java.util.List;

public interface IStudent {
    public OutStudentDTOSimple getStudent(String id, String outputType) throws Exception;
    public List<OutStudentDTOSimple> getStudents(String outputType) throws Exception;
//    public OutStudentDTOComplex getStudentCompleto(String id) throws UserNotFoundException;
    public void addStudent(InpStudentDTO studentEntrada) throws UserNotFoundException;
    public OutStudentDTOSimple modifyStudent(String id, InpStudentDTO estudianteEntrada) throws UserNotFoundException;
    public void deleteStudent(String id);
}
