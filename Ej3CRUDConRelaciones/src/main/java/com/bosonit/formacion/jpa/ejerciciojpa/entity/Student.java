package com.bosonit.formacion.jpa.ejerciciojpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="estudiantes")
@Getter
@Setter
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id_student;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona") //Este es el nombre del campo que tiene la relacion con la tabla direccion (es decir la FK de Persona)
    private Persona persona;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_profesor")
    private Profesor profesor;

    @Column(name = "num_hours_week")
    private int num_hours_week;

    @Column(name = "coments")
    private String coments;

    @NotNull
    @Column(name = "branch")
    private String branch;

    @ManyToMany(mappedBy = "studients")
    private List<Estudiante_Asignatura> estudios = new ArrayList<>();
}
