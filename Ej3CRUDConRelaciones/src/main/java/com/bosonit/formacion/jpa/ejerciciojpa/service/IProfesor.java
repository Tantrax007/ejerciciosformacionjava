package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpProfesorDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutProfesorDTOSimple;

import java.util.List;

public interface IProfesor {
    public OutProfesorDTOSimple getProfesor(String id, String outputType) throws Exception;
    public List<OutProfesorDTOSimple> getProfesores(String outputType) throws Exception;
    public void addProfesor(InpProfesorDTO profesorEntrada);
    public OutProfesorDTOSimple modifyProfesor(String id, InpProfesorDTO profesorEntrada);
    public void deleteProfesor(String id);
}
