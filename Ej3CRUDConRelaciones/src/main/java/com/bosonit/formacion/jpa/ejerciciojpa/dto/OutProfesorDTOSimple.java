package com.bosonit.formacion.jpa.ejerciciojpa.dto;

import com.bosonit.formacion.jpa.ejerciciojpa.entity.Profesor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;


@Component
@Getter
@Setter
@NoArgsConstructor
public class OutProfesorDTOSimple {
    private String id_profesor;
    private int id_persona;
    private String coments;
    private String branch;

    public OutProfesorDTOSimple(Profesor profesor){
        setId_profesor(profesor.getId_profesor());
        setId_persona(profesor.getPersona().getId_persona());
        setComents(profesor.getComents());
        setBranch(profesor.getBranch());
    }
}
