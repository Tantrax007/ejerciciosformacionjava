package com.bosonit.formacion.spring.springjava.controllers;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping({"/", ""})
    public String home(){
        return "redirect:/app/index";
//        return "forward:/app/index"; Con esta opcion nos redirige a la pagina que indicamos pero sin reiniciar la request por lo que no cambia la URL del navegador
//        pero OJO porque no podemos hacer forward de paginas externas, solo podemos de controladores internos
    }
}
