package com.bosonit.formacion.spring.springjava.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/variables")
public class EjemploVariablesRuta {

    @GetMapping(value="/")
    public String index(Model model){
        model.addAttribute("titulo", "Enviar parametros por la ruta (@PathVariable)");
        model.addAttribute("nombre", "Alfonso Sexto el feo");
        return "/variables/index";
    }

    @GetMapping(value="/string/{texto}")
    public String variables(@PathVariable(value="texto") String textoAlter, /* Como la variable no se llama igual que la dinamica del path tenemos que especificar su nombre */ Model model){
        model.addAttribute("titulo", "Recibir parametros de la ruta (@PathVariable)");
        model.addAttribute("resultado", "El texto enviado en la ruta es: " + textoAlter);
        return "variables/ver";
    }

//    @GetMapping(value="/string/{texto}")
//    public String variables(@PathVariable String texto /* Si la variable se llama igual que la variable dinamica del path no es necesario especificarlo */){
//
//    }

    @GetMapping(value="/string/{texto}/{numero}")
    public String variables(@PathVariable(name="texto") String textoAlter,@PathVariable int numero, Model model){
        model.addAttribute("titulo", "Recibir parametros de la ruta (@PathVariable)");
        model.addAttribute("resultado", "El texto enviado en la ruta es: '" + textoAlter + "' y el numero es: " + numero);
        return "variables/ver";
    }
}