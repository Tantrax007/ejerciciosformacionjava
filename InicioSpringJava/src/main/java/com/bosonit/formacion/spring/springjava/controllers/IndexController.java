package com.bosonit.formacion.spring.springjava.controllers;

import com.bosonit.formacion.spring.springjava.models.Usuario;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
//Ruta de primer nivel del controlador
@RequestMapping("/app") //Con esta estamos seteando que para acceder a esta clase primero tenemos que empezar la ruta por "/app" es decir: localhost:8090/app
public class IndexController {

    //Definimos los valores de las variables en el fichero application.properties y los inyectamos con @Value()
    @Value("${texto.indexController.index.titulo}")
    private String textoIndex;

    @Value("${texto.indexController.perfil.titulo}")
    private String textoPerfil;

    @Value("${texto.indexController.listar.titulo}")
    private String textoListar;

//    @GetMapping({"/index", "/", "/holaSpring"})
//    public String index(){
//        return "index";
//    }


    @GetMapping({"/index", "/", "/holaSpring"}) //localhost:8090/app/index || localhost:8090/app/ || localhost:8090/app/holaSpring
    public String index(Model model){
        model.addAttribute("titulo", textoIndex);
        return "index";
    }

//    @GetMapping({"/index", "/", "/holaSpring"})
//    public String index(ModelMap model){
//        model.addAttribute("titulo", "hola Spring con ModelMap");
//        return "index";
//    }

//    @GetMapping({"/index", "/", "/holaSpring"})
//    public String index(Map<String, Object> map){
//        map.put("titulo", "hola Spring con map");
//        return "index";
//    }
//
//    @GetMapping({"/index", "/", "/holaSpring"})
//    public ModelAndView index(ModelAndView mav){
//        mav.addObject("titulo", "hola Spring con ModelAndView"); //Valores que vamos a cargar
//        mav.setViewName("index"); //Lo que le vamos a mandar al usuario
//        return mav;
//    }

    @GetMapping(value="/perfil")
    public String perfil(Model model){

        Usuario usuario = new Usuario("Nahuel", "Omar", "hola");
        usuario.setEmail("nahuelelmejor91@gmail.com");
        model.addAttribute("usuario", usuario);
        model.addAttribute("titulo", textoPerfil + usuario.getNombre());

        return "perfil";
    }

    @GetMapping(value="/listar")
    public String listar(Model model){

        model.addAttribute("titulo", textoListar);

        return "listar";
    }

    @ModelAttribute("usuarios") //Con esta etiqueta definimos que este metodo va a devolver unos valores a la vista, accesible desde cualquier metodo (handler)
    public List<Usuario> poblarUsuarios(){ //Por ejemplo, puede ser util para cuando vamos a requerir datos que se pueden necesitar en varias vistas o metodos
        List<Usuario> usuarios = new ArrayList<>();

        usuarios.add(new Usuario("Javer", "Mena", "javier.mena@bosonit.com"));
        usuarios.add(new Usuario("Alba", "Mendoza", "alba.mendoza@bosonit.com"));
        usuarios.add(new Usuario("Cristina", "Perez", "cistina.perez@bosonit.com"));
        usuarios.add(new Usuario("Ana", "Garcia", "ana.garcia@bosonit.com"));
        usuarios.add(new Usuario("Pablo", "Motos", "pablo.motos@bosonit.com"));

        return usuarios;
    }
}
