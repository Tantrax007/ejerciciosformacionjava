package com.bosonit.formacion.spring.springjava.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value="/params")
public class EjemploParamsController {

    @GetMapping(value="/")
    public String index(){
        return "params/index";
    }

    @GetMapping("/string")
    public String param(@RequestParam(value="texto", defaultValue = "algun valor...") String texto, Model model){
        model.addAttribute("parametro", "El parametro enviado es: " + texto);
        return "params/ver";
    }

    @GetMapping("/mix-params")
    public String mixParam(@RequestParam String saludo, @RequestParam int numero, Model model){
        model.addAttribute("parametro", "El saludo enviado es '" + saludo + "' y el numero es '" + numero + "'");
        return "params/ver";
    }

    @GetMapping("/httprequest")
    public String httpRequest(HttpServletRequest request, Model model){
        int numero = 0;
        String saludo = request.getParameter("saludo");

        try{
            numero = Integer.parseInt(request.getParameter("numero"));
        }
        catch(NumberFormatException nfe){
            numero = 0;
        }

        model.addAttribute("parametro", "El saludo enviado es '" + saludo + "' y el numero es '" + numero + "'");
        return "params/ver";
    }
}
