import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var temperatura = 8;
        if(temperatura < 20)
            System.out.println("Momento de poner la calefaccion");
        else
            System.out.println("Momento de poner el aire acondicionado");
    }
}
