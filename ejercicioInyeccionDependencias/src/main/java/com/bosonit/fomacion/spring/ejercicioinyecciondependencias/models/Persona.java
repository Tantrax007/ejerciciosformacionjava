package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public class Persona {
    private String nombre, poblacion, edad;

    public Persona() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getEdad() {
        return edad;
    }

    public void     setEdad(String edad) {
        this.edad = edad;
    }
}
