package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Persona;
import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Personas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/controlador/bean/")
public class Controlador {
    @Autowired
    @Qualifier("persona1")
    private Persona persona1;

    @Autowired
    @Qualifier("persona2")
    private Persona persona2;

    @Autowired
    @Qualifier("persona3")
    private Persona persona3;

    @GetMapping(value="/{numBean}")
    public Persona getPersona(@PathVariable String numBean){
        return switch (numBean){
            case "bean1" -> persona1;
            case "bean2" -> persona2;
            case "bean3" -> persona3;
            default -> null;
        };
    }
}
