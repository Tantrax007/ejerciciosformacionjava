package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.Services;
import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Ciudad;
import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="/controlador2")
public class Controlador2 {

    //Todo: Preguntas
    // --> Como se haría para pasar las llamadas que tengo mal hechas a interfaces

    @Autowired
    private PersonaService personaService;

    @Autowired
    private IServices ciudades;

    @GetMapping(value="/getPersona")
    public @ResponseBody Persona getPersona(){
        if (personaService.getPersona().getEdad() == null){
            personaService.getPersona().setEdad(null); //Si no hay persona no podemos multiplcar por dos
        }
        else {
            personaService.getPersona().setEdad(Integer.parseInt(personaService.getPersona().getEdad()) * 2 + "");
        }
        return personaService.getPersona();
    }

    @GetMapping(value="/getCiudad")
    public List<Ciudad> getCiudades(){
        return ciudades.getCiudad();
    }
}
