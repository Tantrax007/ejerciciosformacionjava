package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Persona;
import org.springframework.stereotype.Component;

@Component
public class PersonaImpl implements  PersonaService{
    Persona persona= new Persona();

    @Override
    public void setPersona(String nombre, String poblacion,String edad)
    {
        persona.setNombre(nombre);
        persona.setPoblacion(poblacion);
        persona.setEdad(edad);
    }
    @Override
    public Persona getPersona()
    {
        return persona;
    }
}
