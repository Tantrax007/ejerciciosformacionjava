package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Personas {
    @Bean("persona1")
    public Persona getPersona1(){
        Persona persona1 = new Persona();
        persona1.setNombre("Alberto Garcia");
        persona1.setEdad("20");
        persona1.setPoblacion("Arnedo");
        return persona1;
    }

    @Bean("persona2")
    public Persona getPersona2(){
        Persona persona2 = new Persona();
        persona2.setNombre("Manuel Garcia");
        persona2.setEdad("32");
        persona2.setPoblacion("Madrid");
        return persona2;
    }

    @Bean("persona3")
    public Persona getPersona3(){
        Persona persona3 = new Persona();
        persona3.setNombre("Jose Perez");
        persona3.setEdad("12");
        persona3.setPoblacion("Villamediana");
        return persona3;
    }
}
