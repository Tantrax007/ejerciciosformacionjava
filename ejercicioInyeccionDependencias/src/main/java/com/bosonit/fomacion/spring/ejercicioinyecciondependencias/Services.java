package com.bosonit.fomacion.spring.ejercicioinyecciondependencias;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers.IServices;
import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Ciudad;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class Services implements IServices {
    private List<Ciudad> ciudad;

    @Bean
    public List<Ciudad> getCiudad() {
        ciudad = new ArrayList<Ciudad>();
        return ciudad;
    }
}