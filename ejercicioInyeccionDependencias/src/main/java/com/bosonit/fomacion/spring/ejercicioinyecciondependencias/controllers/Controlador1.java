package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.Services;
import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Ciudad;
import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value="/controlador1")
public class Controlador1 {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private Ciudad ciudad;

    @Autowired
    private Services listaCiudades;

    @GetMapping(value="/addPersona")
    public Persona getBaseUrl(@RequestHeader String nombre,@RequestHeader String poblacion,@RequestHeader String edad ) {

        personaService.setPersona(nombre,poblacion,edad);

        return personaService.getPersona();
    }

    @PostMapping(value="/addCiudad")
    public void addCiudad(@RequestBody Ciudad ciudad){

        this.ciudad.setNombre(ciudad.getNombre());
        this.ciudad.setNumeroHabitantes(ciudad.getNumeroHabitantes());

        listaCiudades.getCiudad().add(this.ciudad);
    }
}
