package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Persona;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("fichero")
public class PersonaImpl2 implements  PersonaService{
    Persona persona= new Persona();

    @Override
    public void setPersona(String nombre, String poblacion,String edad)
    {
        persona.setNombre(nombre);
    }
    @Override
    public Persona getPersona()
    {
        return persona;
    }
}
