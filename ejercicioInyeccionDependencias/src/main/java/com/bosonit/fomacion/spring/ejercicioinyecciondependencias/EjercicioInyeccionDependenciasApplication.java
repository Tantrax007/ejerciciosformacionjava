package com.bosonit.fomacion.spring.ejercicioinyecciondependencias;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Ciudad;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class EjercicioInyeccionDependenciasApplication {

    public static void main(String[] args) {
        SpringApplication.run(EjercicioInyeccionDependenciasApplication.class, args);
    }
}
