package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Ciudad;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

public interface IServices {
    @Bean
    public List<Ciudad> getCiudad();
}
