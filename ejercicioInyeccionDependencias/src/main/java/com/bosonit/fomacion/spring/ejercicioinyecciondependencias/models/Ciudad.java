package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models;

import org.springframework.stereotype.Component;

@Component
public class Ciudad {
    private String Nombre;
    private int numeroHabitantes;

    public Ciudad() {
    }

    public Ciudad(String nombre, int numeroHabitantes) {
        Nombre = nombre;
        this.numeroHabitantes = numeroHabitantes;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getNumeroHabitantes() {
        return numeroHabitantes;
    }

    public void setNumeroHabitantes(int numeroHabitantes) {
        this.numeroHabitantes = numeroHabitantes;
    }
}
