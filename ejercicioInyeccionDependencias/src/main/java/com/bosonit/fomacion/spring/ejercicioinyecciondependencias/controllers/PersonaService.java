package com.bosonit.fomacion.spring.ejercicioinyecciondependencias.controllers;

import com.bosonit.fomacion.spring.ejercicioinyecciondependencias.models.Persona;

public interface PersonaService {
    public void setPersona(String nombre, String poblacion,String edad);
    public Persona getPersona();

}
