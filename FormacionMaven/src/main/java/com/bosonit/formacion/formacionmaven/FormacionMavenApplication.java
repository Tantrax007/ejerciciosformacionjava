package com.bosonit.formacion.formacionmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormacionMavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(FormacionMavenApplication.class, args);
    }

}
