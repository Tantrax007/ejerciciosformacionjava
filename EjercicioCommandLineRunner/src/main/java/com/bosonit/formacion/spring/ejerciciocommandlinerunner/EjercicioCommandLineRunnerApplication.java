package com.bosonit.formacion.spring.ejerciciocommandlinerunner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class EjercicioCommandLineRunnerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        log.info("Iniciando aplicación");
        SpringApplication.run(EjercicioCommandLineRunnerApplication.class, args);
        log.info("Aplicación finalizada");
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Estamos dentro del Runner");
        for (int i = 0; i < 5; i++) {
            log.info("Vuelta: " + i);
        }
    }
}
