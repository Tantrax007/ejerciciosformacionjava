package com.bosonit.formacion.spring.aprendiendoresponseentity.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HttpStatusController {
    @GetMapping("/ok")
    ResponseEntity<String> hello() { //Devolvemos un ResponseEntity<String> de tipo String con error 404
        return new ResponseEntity<>(HttpStatus.OK + ": Respuesta estándar para peticiones correctas.", HttpStatus.OK); //Mandamos en el body el status ok(200).
    }

    @GetMapping("/notFound")
    ResponseEntity<String> NotFound() { //Devolvemos un ResponseEntity<String> de tipo String con error 404
        return new ResponseEntity<>(HttpStatus.NOT_FOUND + ": Respuesta estándar para peticiones correctas.", HttpStatus.NOT_FOUND); //Mandamos en el body el status ok(200).
    }

    @GetMapping("/error")
    ResponseEntity<String> DemasiadoPronto() { //Devolvemos un ResponseEntity<String> de tipo String con error 425
        return new ResponseEntity<>(HttpStatus.TOO_EARLY + ": Respuesta estándar para peticiones correctas.", HttpStatus.TOO_EARLY); //Mandamos en el body el status tooEarly(425).
    }

    @GetMapping(value="/teapot")
    ResponseEntity<String> noSoyUnaCafetera(){
        return new ResponseEntity<>(HttpStatus.I_AM_A_TEAPOT + ": No puedo hacer cafe porque soy una tetera.", HttpStatus.I_AM_A_TEAPOT);
    }

    @GetMapping(value="/badRequest")
    ResponseEntity<String> badRequest(){
        return ResponseEntity.badRequest().body("Esto es una mala peticion"); //Podemos utilizar estos metodos estaticos para no tener que hacer lo de abajo
//        return new ResponseEntity<>("Esto es una mala peticion", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value="/customHeaders")
    ResponseEntity<String> customHeaders(){
        return ResponseEntity.ok()
                .header("Esto es un Header", "Esto es el cuerpo del header")
                .body("Esto es el cuerpo del HTTPResponse");
    }
}
