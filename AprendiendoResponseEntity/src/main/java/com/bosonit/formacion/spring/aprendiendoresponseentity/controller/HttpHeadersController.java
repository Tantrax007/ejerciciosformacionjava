package com.bosonit.formacion.spring.aprendiendoresponseentity.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpResponse;

@RestController
public class HttpHeadersController {
    @GetMapping(value="/headers")
    ResponseEntity<String> helloHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Hola", "Hola Mundo!");
        headers.add("Web", "javadesde0.com");

        return new ResponseEntity<>(
                "Mensaje de la descripcion", headers, HttpStatus.OK
        );
    }
}
