package com.bosonit.formacion.demodocker.aplication;

import com.bosonit.formacion.demodocker.domain.Usuario;
import com.bosonit.formacion.demodocker.infrastructure.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(value="/getUsuario/{id}")
    public Usuario getUsuairo(@PathVariable int id) throws Exception{
        return usuarioService.getUsuarioById(id);
    }

    @PostMapping(value="/insertarUsuario")
    public boolean insertarUsuario(@RequestBody Usuario usuario){
        Optional<Usuario> usuarioEncontrado = usuarioService.postUsuario(usuario);
        return usuarioEncontrado.isPresent();
    }
}
