package com.bosonit.formacion.demodocker.infrastructure;

import com.bosonit.formacion.demodocker.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Override
    public Usuario getUsuarioById(int id) throws Exception {
        return usuarioRepositorio.findById(id).orElseThrow(() -> new Exception("No se ha encontrado el usuario"));
    }

    @Override
    public Optional<Usuario> postUsuario(Usuario usuario) {
        try{
            usuarioRepositorio.save(usuario);
            return Optional.of(usuario);
        }
        catch (Exception e){
            System.out.println("A ocurrido un error al intentar guardar el usaurio");
        }
        return Optional.empty();
    }
}
