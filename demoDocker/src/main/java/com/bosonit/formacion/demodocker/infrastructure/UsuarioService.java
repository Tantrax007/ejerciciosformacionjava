package com.bosonit.formacion.demodocker.infrastructure;

import com.bosonit.formacion.demodocker.domain.Usuario;

import java.util.Optional;

public interface UsuarioService {
    Usuario getUsuarioById(int id) throws Exception;
    Optional<Usuario> postUsuario(Usuario usuario);
}
