package com.bosonit.formacion.demodocker.infrastructure;

import com.bosonit.formacion.demodocker.domain.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepositorio extends CrudRepository<Usuario, Integer> {
}
