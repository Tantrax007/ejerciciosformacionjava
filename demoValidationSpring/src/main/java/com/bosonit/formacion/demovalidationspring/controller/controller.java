package com.bosonit.formacion.demovalidationspring.controller;

import com.bosonit.formacion.demovalidationspring.entity.Estudiante;
import com.bosonit.formacion.demovalidationspring.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
public class controller {

    @Autowired
    EstudianteService estudianteService;

    @PostMapping(value="/agregar")
    public ResponseEntity<String> addEstudiante(@Valid @RequestBody Estudiante estudiante){
        estudianteService.addEstudiante(estudiante);
        return ResponseEntity.ok().body("Todo correcto");
    }

//    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
//        Map<String, String> errors = new HashMap<>();
//        ex.getBindingResult().getAllErrors().forEach((error) -> {
//            String fieldName = ((FieldError) error).getField();
//            String errorMessage = error.getDefaultMessage();
//            errors.put(fieldName, errorMessage);
//        });
//        return errors;
//    }
}
