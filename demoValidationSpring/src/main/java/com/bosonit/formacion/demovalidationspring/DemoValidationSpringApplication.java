package com.bosonit.formacion.demovalidationspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoValidationSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoValidationSpringApplication.class, args);
    }

}
