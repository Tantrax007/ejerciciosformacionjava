package com.bosonit.formacion.demovalidationspring.repository;

import com.bosonit.formacion.demovalidationspring.entity.Estudiante;
import org.springframework.data.repository.CrudRepository;

public interface EstudianteRepository extends CrudRepository<Estudiante, Long> { }
