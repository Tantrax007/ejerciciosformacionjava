package com.bosonit.formacion.demovalidationspring.service;

import com.bosonit.formacion.demovalidationspring.entity.Estudiante;

public interface EstudianteService {
    void addEstudiante(Estudiante estudiante);
}
