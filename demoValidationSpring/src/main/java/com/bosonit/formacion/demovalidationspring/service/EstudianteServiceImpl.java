package com.bosonit.formacion.demovalidationspring.service;

import com.bosonit.formacion.demovalidationspring.entity.Estudiante;
import com.bosonit.formacion.demovalidationspring.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstudianteServiceImpl implements EstudianteService {

    @Autowired
    EstudianteRepository estudianteRepository;

    @Override
    public void addEstudiante(Estudiante estudiante) {
        estudianteRepository.save(estudiante);
    }
}
