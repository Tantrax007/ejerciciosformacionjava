package com.bosonit.formacion.demovalidationspring.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull(message = "El nombre no puede ser nulo")
    private String nombre;

    @Min(value = 10, message = "La edad tiene que ser mayor a 10")
    @Max(value = 90, message = "La edad tiene que ser menor de 90")
    @NotNull(message = "La edad no puede ser nulo")
    private int edad;

    @Email(message = "Tiene que ser un email válido")
    @NotNull(message = "El email no puede ser nulo")
    private String email;

    public Estudiante() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}