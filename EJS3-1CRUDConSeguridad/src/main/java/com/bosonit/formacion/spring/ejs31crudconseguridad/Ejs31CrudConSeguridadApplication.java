package com.bosonit.formacion.spring.ejs31crudconseguridad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejs31CrudConSeguridadApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ejs31CrudConSeguridadApplication.class, args);
    }

}
