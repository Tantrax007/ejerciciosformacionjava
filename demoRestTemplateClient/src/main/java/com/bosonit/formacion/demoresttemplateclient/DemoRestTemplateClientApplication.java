package com.bosonit.formacion.demoresttemplateclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRestTemplateClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoRestTemplateClientApplication.class, args);
    }

}
