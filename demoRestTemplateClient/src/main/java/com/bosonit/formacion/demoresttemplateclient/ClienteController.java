package com.bosonit.formacion.demoresttemplateclient;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@RestController
public class ClienteController {
    @GetMapping(value="/obtenerUsuario")
    public String obtenerUsuario() {
        try{
            ResponseEntity<Customer> responseEntity = new RestTemplate().getForEntity("http://localhost:8081/getUsuario", Customer.class);
            return "Exito en la peticion";
        }
        catch(HttpClientErrorException k1){
            return "El servidor no respondio con un 200, sino que lo hizo con un: " + k1.getStatusCode() +
                    " Causa: "+ k1.getResponseBodyAsString();
        }
        catch(RestClientException k){
            return "El servidor no ha respondido. La causa es:\n " + k.getMessage();
        }
    }
}