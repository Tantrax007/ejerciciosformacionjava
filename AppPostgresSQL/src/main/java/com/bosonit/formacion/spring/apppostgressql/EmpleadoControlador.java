package com.bosonit.formacion.spring.apppostgressql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/employee")
public class EmpleadoControlador {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value="/getEmpleado/{id}")
    public Employee getEmployee(@PathVariable String id) throws Exception{
        return employeeService.getById(id);
    }

    @GetMapping(value="/getEmpleados")
    public List<Employee> getEmployees(){
        return employeeService.getAllEmployees();
    }

    @PostMapping(value="/addEmpleado")
    public void addEmpleado(Employee empleado){
        employeeService.createEmployee(empleado);
    }

    @PutMapping(value="/updateEmpleado")
    public void updateEmpleado(Employee employee){
        employeeService.updateEmployee(employee);
    }

    @DeleteMapping(value="/deleteEmpleado/{id}")
    public void deleteEmpleado(@PathVariable String id){
        employeeService.deleteEmployee(id);
    }
}
