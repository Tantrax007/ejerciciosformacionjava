package com.bosonit.formacion.spring.apppostgressql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees(){
        List<Employee> empleados = new ArrayList<>();
        employeeRepository.findAll().forEach(empleados::add);
        return empleados;
    }

    public Employee getById(String id) throws Exception{
        return employeeRepository.findById(id).orElseThrow(() -> new Exception("No existe este usuario"));
    }

    public void createEmployee(Employee employee){
        employeeRepository.save(employee);
    }

    public void updateEmployee(Employee employee){
        employeeRepository.save(employee);
    }

    public void deleteEmployee(String id){
        employeeRepository.deleteById(id);
    }
}
