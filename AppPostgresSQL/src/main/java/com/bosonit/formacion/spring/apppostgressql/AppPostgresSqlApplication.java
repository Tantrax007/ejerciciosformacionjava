package com.bosonit.formacion.spring.apppostgressql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppPostgresSqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppPostgresSqlApplication.class, args);
    }

}
