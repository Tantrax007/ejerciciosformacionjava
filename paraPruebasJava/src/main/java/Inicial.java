import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Inicial {
    public static void main(String[] args) {
        List<String> lista = List.of("Hola", "", "Java", "", "Concurrente", "Streams", "", "Java11", "Java12");
        Stream<String> stream = lista.stream(); //Convertimos la lista
        long conteo = stream.filter(String::isEmpty).count();
        System.out.println(conteo);
    }
}
