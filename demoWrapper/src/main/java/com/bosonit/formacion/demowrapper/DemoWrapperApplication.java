package com.bosonit.formacion.demowrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoWrapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoWrapperApplication.class, args);
    }

}
