package com.bosonit.formacion.spring.ejerciciocrud;

import com.bosonit.formacion.spring.ejerciciocrud.domain.Persona;

import java.util.Map;

public interface IPersona {
    public void addPersona(Persona persona); //C
    public Map<Integer,Persona> getPersona(); //R
    public void updatePersona(int id, Persona persona); //U
    public void deletePersona(int id); //D
}
