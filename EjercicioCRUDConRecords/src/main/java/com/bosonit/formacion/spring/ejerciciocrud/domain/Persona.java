package com.bosonit.formacion.spring.ejerciciocrud.domain;

public record Persona(String nombre, String poblacion, int edad) {}
