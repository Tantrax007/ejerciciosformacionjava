package com.bosonit.formacion.spring.ejerciciocrud.domain;

import com.bosonit.formacion.spring.ejerciciocrud.IPersona;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PersonaService implements IPersona {
    private static int id;
    Map<Integer, Persona> personas = new HashMap<>();


    @Override
    public void addPersona(Persona persona) {
        id += 1;
        Persona addPersona = new Persona(persona.nombre(), persona.poblacion(), persona.edad());
        personas.put(id, addPersona);
    }

    @Override
    public Map<Integer, Persona> getPersona() {
        return personas;
    }

    @Override
    public void updatePersona(int id, Persona persona) { //Todo Podria ser un boolean para retornar si se ha podido modificar o no
        if (personas.containsKey(id)){ //Si el id que obtenemos existe
                Persona parAEditar = personas.get(id); //Obtenemos la persona
                personas.put(id, parAEditar); //Actualizamos en el HashMap
        }
    }

    @Override
    public void deletePersona(int id) { //Todo podria ser un boolean para retornar si se ha podido eliminar la persona o no
        if (personas.containsKey(id)) { //Si la persona con ese Id existe
            personas.remove(id);
        }
    }
}