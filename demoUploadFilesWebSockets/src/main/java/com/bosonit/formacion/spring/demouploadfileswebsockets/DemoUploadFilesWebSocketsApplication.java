package com.bosonit.formacion.spring.demouploadfileswebsockets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoUploadFilesWebSocketsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoUploadFilesWebSocketsApplication.class, args);
    }

}
