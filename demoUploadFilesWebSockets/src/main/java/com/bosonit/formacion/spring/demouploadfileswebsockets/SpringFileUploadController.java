package com.bosonit.formacion.spring.demouploadfileswebsockets;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Controller
public class SpringFileUploadController {

    @GetMapping(value="/index")
    public String hello(){
        return "uploader";
    }

    @PostMapping(value="/upload")
    public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file) { //Este endpoint lo que sta esperando es un fichero de tipo Multipartfile
        String fileName = file.getOriginalFilename(); //Obtenemos el nombre limpio del fichero que se ha mandado


        try{
            file.transferTo(new File("C:\\upload\\" + fileName));
        }
        catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build(); //En el caso de que ocurra algo con la subida del fichero
        }

        return ResponseEntity.ok("File uploaded successfully.");
    }
}
