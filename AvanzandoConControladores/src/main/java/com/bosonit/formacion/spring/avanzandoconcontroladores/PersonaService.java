package com.bosonit.formacion.spring.avanzandoconcontroladores;

import org.springframework.stereotype.Service;

@Service
public class PersonaService implements IPersona{
    Persona persona = new Persona();

    @Override
    public void setPersona(String nombre, int edad, String poblacion) {
        persona.setNombre(nombre);
        persona.setEdad(edad);
        persona.setPoblacion(poblacion);
    }

    @Override
    public Persona getPersona() {
        return persona;
    }
}
