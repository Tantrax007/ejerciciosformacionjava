package com.bosonit.formacion.spring.avanzandoconcontroladores;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController //Con esto nos evitamos tener que poner en cada metodo el @ResponseBody
@RequestMapping("/controller")
public class GreetingController {
    @Autowired
    private IPersona persona;

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) { //Nosotros devolvemos un objeto pero SpringBoot se encarga de parsearlo a JSON
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @GetMapping("/user/{id}")
    public String variables(@PathVariable String id) {
        return "Hola " + id;
    }

    @PostMapping("/sendObject")
    public Persona addPersona(@RequestBody Persona persona) {
        this.persona.setPersona(persona.getNombre(), persona.getEdad(), persona.getPoblacion());
        return this.persona.getPersona();
    }

    @PutMapping("/post")
    public String putParams(@RequestParam(name = "var1") int var1, @RequestParam(name = "var2") int var2) {
        return "var1 = " + var1 + ", var2 = " + var2;
    }
}
