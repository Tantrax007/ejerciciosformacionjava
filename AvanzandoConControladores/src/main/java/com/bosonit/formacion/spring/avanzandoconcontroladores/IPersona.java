package com.bosonit.formacion.spring.avanzandoconcontroladores;

public interface IPersona {
    public void setPersona(String nombre, int edad, String poblacion);
    public Persona getPersona();
}
