package com.bosonit.formacion.spring.avanzandoconcontroladores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvanzandoConControladoresApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvanzandoConControladoresApplication.class, args);
    }

}
