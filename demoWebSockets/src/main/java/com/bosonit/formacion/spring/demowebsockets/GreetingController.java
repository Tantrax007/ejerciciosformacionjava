package com.bosonit.formacion.spring.demowebsockets;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class GreetingController {

    @MessageMapping(value="hello") //Cuando se mande un mensaje a este endpoint
    @SendTo("/topic/greetings")
    public Greeting greeting(Message message) throws Exception{
        Thread.sleep(1000); //Agregamos un delay como si estubieramos escribiendo
        //El delay lo hemos agregado para demostrar que esto es una comunicacion asincrona y que el cliente puede seguir con lo que este haciendo mientas el
        //servidor procesa el mensaje y la respuesta
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }


}
