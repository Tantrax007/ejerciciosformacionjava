package com.bosonit.formacion.spring.demowebsockets;

public class Message {
    private String name;
    private String message;

    public Message(){

    }

    public Message(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }
}
