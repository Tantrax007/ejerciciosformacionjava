package com.bosonit.formacion.microservicios.moviecatalogservice;

import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value="/catalog")
public class MovieCatalogResource {

    @GetMapping(value="/{id}")
    public List<CatalogItem> getCatalog(@PathVariable("id") String userId) {
        return Collections.singletonList(
                new CatalogItem("Titanic", "The worst movie", 6)
        );
    }
}
