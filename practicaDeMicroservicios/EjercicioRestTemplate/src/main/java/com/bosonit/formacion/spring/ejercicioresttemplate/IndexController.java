package com.bosonit.formacion.spring.ejercicioresttemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value="/getPersona")
public class IndexController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value="/{id}")
    public OutProfesorDTOSimple getRemotPersona(@PathVariable String id){
        return restTemplate.getForObject("http://localhost:8081/profesor/getProfesor/3", OutProfesorDTOSimple.class);
    }
}
