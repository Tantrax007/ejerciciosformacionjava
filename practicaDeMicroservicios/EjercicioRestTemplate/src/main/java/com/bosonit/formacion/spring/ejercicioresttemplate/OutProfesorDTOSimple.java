package com.bosonit.formacion.spring.ejercicioresttemplate;


public class OutProfesorDTOSimple {
    private String id_profesor;
    private int id_persona;
    private String coments;
    private String branch;

    public OutProfesorDTOSimple(){

    }

    public OutProfesorDTOSimple(String id_profesor, int id_persona, String coments, String branch) {
        this.id_profesor = id_profesor;
        this.id_persona = id_persona;
        this.coments = coments;
        this.branch = branch;
    }

    public String getId_profesor() {
        return id_profesor;
    }

    public void setId_profesor(String id_profesor) {
        this.id_profesor = id_profesor;
    }

    public int getId_persona() {
        return id_persona;
    }

    public void setId_persona(int id_persona) {
        this.id_persona = id_persona;
    }

    public String getComents() {
        return coments;
    }

    public void setComents(String coments) {
        this.coments = coments;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
