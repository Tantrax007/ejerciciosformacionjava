package com.bosonit.formacion.jpa.ejerciciojpa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

//@ControllerAdvice <-- Esta etiqueta deriva de @Controller y se usa para aquellas clases que tratan excepciones
//@RestController <-- Esta trata los controladores REST que lanzan excepciones
@RestControllerAdvice
public class RespuestaCustomOperacionNoProcesable {
    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<CustomError> handleNotFoundException(UnProcesableException up, WebRequest request) {
        CustomError exceptionResponse = new CustomError(new Date(), up.getMessage(),
                request.getDescription(false), HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
        return new ResponseEntity<CustomError>(exceptionResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
