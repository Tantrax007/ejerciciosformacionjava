package com.bosonit.formacion.jpa.ejerciciojpa.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@Component
public class InpPersonaDTO {
    private String usuario;
    private String password;
    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private String active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;
}
