package com.bosonit.spring.formacion.mongodemo;

import com.bosonit.spring.formacion.mongodemo.model.Employee;
import com.bosonit.spring.formacion.mongodemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @PostMapping(value="/add")
    public Employee saveEmployee(@RequestBody Employee newEmployee){
        return employeeService.save(newEmployee);
    }

    @GetMapping(value="/getOne/{id}")
    public Employee getOneEmployee(@PathVariable(name="id") Integer employeeId){
        return employeeService.getOne(employeeId);
    }

    @GetMapping(value="/getAll")
    public List<Employee> getAllEmployees(){
        return employeeService.getAllEmployees();
    }

    @PutMapping(value="/updateEmployee")
    public Employee updateEmployee(@RequestBody Employee updateEmployee){
        return employeeService.updateEmployee(updateEmployee);
    }

    @DeleteMapping(value="/delete/{id}")
    public void deleteEmployee(@PathVariable String id){
        employeeService.removeEmployee(id);
    }

}