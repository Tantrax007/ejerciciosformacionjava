package com.bosonit.spring.formacion.mongodemo.repository;

import com.bosonit.spring.formacion.mongodemo.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ImportAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)
public class EmployeeRepository {
    @Autowired
    MongoTemplate mongoTemplate;

    public Employee save(Employee emp){
        return mongoTemplate.save(emp);
    }

    public Employee getOne(int employeeId){
        Query query = new Query(Criteria.where("_id").is(employeeId)); //{"_id":"3"}
        return mongoTemplate.findOne(query,Employee.class);
    }

    public List<Employee> getAllEmployees(){
        return mongoTemplate.findAll(Employee.class);//find({})
    }

    public Employee updateEmployee(Employee updateEmployee){
        return mongoTemplate.save(updateEmployee); //Actualizacion Upsert
    }

    public void removeEmployee(String id){
        Query query = new Query(Criteria.where("_id").is(id)); //db.collection.deleteOne({"_id":id})
        mongoTemplate.remove(query);
    }
}
