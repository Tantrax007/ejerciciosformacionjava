package com.bosonit.spring.formacion.mongodemo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Document //Este es el que define que esta clase es un documento en MongoDB valores posibles: name="" <-- Es el nombre que le podemos dar al documento,
// si no le pasamos este atributo lo que va a coger es el nombre de la clase
public class Employee {
    @Id
    private int id;
    private String firstName;
    private float salary;
    private Address address;
    private Date date;

    public Employee() {
    }

    public Employee(int id, String firstName, float salary, Address address, Date date) {
        this.id = id;
        this.firstName = firstName;
        this.salary = salary;
        this.address = address;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
