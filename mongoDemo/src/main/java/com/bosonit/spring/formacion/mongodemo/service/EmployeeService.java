package com.bosonit.spring.formacion.mongodemo.service;

import com.bosonit.spring.formacion.mongodemo.model.Employee;
import com.bosonit.spring.formacion.mongodemo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    public Employee save(Employee emp){
        emp.setDate(new Date());
        return employeeRepository.save(emp);
    }

    public Employee getOne(int employeeId){
        return employeeRepository.getOne(employeeId);
    }

    public List<Employee> getAllEmployees(){
        return employeeRepository.getAllEmployees();
    }

    public Employee updateEmployee(Employee updateEmployee){
        return employeeRepository.updateEmployee(updateEmployee);
    }

    public void removeEmployee(String id){
        employeeRepository.removeEmployee(id);
    }
}
