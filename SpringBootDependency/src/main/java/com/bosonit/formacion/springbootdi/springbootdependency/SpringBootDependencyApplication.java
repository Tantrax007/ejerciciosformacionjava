package com.bosonit.formacion.springbootdi.springbootdependency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDependencyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDependencyApplication.class, args);
    }

}
