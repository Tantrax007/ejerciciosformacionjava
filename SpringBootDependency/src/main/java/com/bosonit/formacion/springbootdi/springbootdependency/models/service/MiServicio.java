package com.bosonit.formacion.springbootdi.springbootdependency.models.service;

import org.springframework.stereotype.Component;

@Component("MiServicioSimple") //--> Creamos un objeto una sola vez en la aplicacion (Singleton) y esta se distribuye entre los componentes de la app
//pero OJO! porque tienen que estar dentro del paquete base
//@Service --> Funciona igual que el @Component pero este agrega una semantica, algo descriptivo de la clase
public class MiServicio implements IServicio{

    @Override
    public String operacion(){
        return "ejecutando algun proceso importante simple...";
    }
}
