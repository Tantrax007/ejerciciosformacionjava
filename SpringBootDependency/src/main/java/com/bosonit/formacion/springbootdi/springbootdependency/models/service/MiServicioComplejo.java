package com.bosonit.formacion.springbootdi.springbootdependency.models.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component("MiServicioComplejo") //--> Creamos un objeto una sola vez en la aplicacion (Singleton) y esta se distribuye entre los componentes de la app
//pero OJO! porque tienen que estar dentro del paquete base
//@Service --> Funciona igual que el @Component pero este agrega una semantica, algo descriptivo de la clase
@Primary //Con esto le indicamos que la clase a cargar cuando hay varias es esta
public class MiServicioComplejo implements IServicio{

    @Override
    public String operacion(){
        return "ejecutando algun proceso importante complicado...";
    }
}
