package com.bosonit.formacion.springbootdi.springbootdependency.models.service;

public interface IServicio {
    public String operacion();
}
