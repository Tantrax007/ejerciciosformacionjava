package com.bosonit.formacion.springbootdi.springbootdependency.controllers;

import com.bosonit.formacion.springbootdi.springbootdependency.models.service.IServicio;
import com.bosonit.formacion.springbootdi.springbootdependency.models.service.MiServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @Autowired
    @Qualifier("MiServicioComplejo") //Con esto le estamos indicando el nombre del componente que queremos inyectar
    //Inyectamos un objeto creado en el contenedor de Spring que sea del tipo MiServicio
    private IServicio servicio;

    @GetMapping(value= {"", "/", "/index"})
    public String index(Model model){
        model.addAttribute("objeto", servicio.operacion());
        return "index";
    }
}
