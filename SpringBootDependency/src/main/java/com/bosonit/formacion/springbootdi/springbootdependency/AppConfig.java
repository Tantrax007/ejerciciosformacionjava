package com.bosonit.formacion.springbootdi.springbootdependency;

import com.bosonit.formacion.springbootdi.springbootdependency.models.domain.ItemFactura;
import com.bosonit.formacion.springbootdi.springbootdependency.models.domain.Producto;
import com.bosonit.formacion.springbootdi.springbootdependency.models.service.IServicio;
import com.bosonit.formacion.springbootdi.springbootdependency.models.service.MiServicio;
import com.bosonit.formacion.springbootdi.springbootdependency.models.service.MiServicioComplejo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class AppConfig {

    @Bean("miServicioSimple")
    public IServicio registrarServicioSimple(){
        return new MiServicio();
    }

    @Bean("miServicioComplejo")
    public IServicio registrarServicioComplejo(){
        return new MiServicioComplejo();
    }

    @Bean("registrarItemsFactura")
    public List<ItemFactura> registrarItems(){
        Producto producto1 = new Producto("MSI Creator z16", 2500);
        Producto producto2 = new Producto("Sony Alpha Z3", 2354);
        Producto producto3 = new Producto("Materika Galsses", 500);

        ItemFactura linea1 = new ItemFactura(producto1, 2);
        ItemFactura linea2 = new ItemFactura(producto2, 4);
        ItemFactura linea3 = new ItemFactura(producto3, 8);


        return Arrays.asList(linea1, linea2, linea3);
    }
}
