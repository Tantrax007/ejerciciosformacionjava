package com.bosonit.formacion.springbootdi.springbootdependency.controllers;

import com.bosonit.formacion.springbootdi.springbootdependency.models.domain.Factura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/factura")
public class FacturaController {

    @Autowired
    private Factura factura;

    @GetMapping(value="/ver")
    public String ver(Model model){
        model.addAttribute("factura", factura);
        model.addAttribute("titulo", "Ejemplo Factura con inyeccion de dependencia");
        return "factura/ver";
    }
}
