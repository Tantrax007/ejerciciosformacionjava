
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;


public class Inicial {
    public static void main(String[] args) {
        Estudiante estudiante = new Estudiante();
        estudiante.setEdad(20);
        estudiante.setEmail("estudiante.apellido@bosonit.com");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Estudiante>> violations = validator.validate(estudiante);
        violations.forEach(System.out::println);
    }
}
