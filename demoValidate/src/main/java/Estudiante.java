import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;

public class Estudiante {

    @NotNull(message = "El nombre no puede ser nulo")
    private String nombre;

    @Min(value = 10, message = "La edad tiene que ser mayor a 10")
    @Max(value = 90, message = "La edad tiene que ser menor de 90")
    private int edad;

    @Email(message = "Tiene que ser un email válido")
    private String email;

    public Estudiante() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}