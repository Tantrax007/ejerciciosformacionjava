package org.optional.ejemplo;

import java.util.Optional;

public class EjemploOptional {
    public static void main(String[] args) {
        String nombre = "Nahuel", nombre2 = "Jihane", nombre3 = "Ana";
        Optional<String> opt = Optional.of(nombre); //Creamos un optional con of(nombre) desde la variable nombre y no permite valor null
        Optional<String> opt2 = Optional.empty(); //Creamos un optional vacio
        Optional<String> opt3 = Optional.ofNullable(nombre2); //Creamos un optional sobre nombre2 y permite un valor null

        if(opt.isPresent()) { //Comprobamos que el opcional tiene algo
            System.out.println(opt); //Optional=[Nahuel]
        }

        if(!opt2.isEmpty()) { //Comprobamos si el opcional no esta vacio
            System.out.println(opt2);
        }

        if(opt3.isPresent()) {
            System.out.println(opt3);
        }

        nombre = null;

        opt = Optional.of(nombre); //Esto da error porque no podemos crear un Optional con el metodo of() si el valor que vamos a utilizar es null
        opt = Optional.ofNullable(nombre); //Creamos un optional empty ya que el valor que le estamos pasando es null

        opt.ifPresent(string -> System.out.println("Tenemos el valor " + string + " ya que no es null")); //Si el opt tiene algo
        opt.ifPresentOrElse(string -> System.out.println("Tenemos el valor" + string + " ya que no es nulo"), () -> System.out.println("El opcional no esta presenta"));
    }
}
