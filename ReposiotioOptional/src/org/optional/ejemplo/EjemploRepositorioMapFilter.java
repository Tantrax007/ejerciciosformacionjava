package org.optional.ejemplo;

import org.optional.ejemplo.models.Fabricante;
import org.optional.ejemplo.models.Ordenador;
import org.optional.ejemplo.repositorio.OrdenadorRep;
import org.optional.ejemplo.repositorio.Repositorio;

public class EjemploRepositorioMapFilter {
    public static void main(String[] args) {
        Repositorio<Ordenador> repoPc = new OrdenadorRep(); //Obtenemos un nuevo ordenador a traves del repo

        String fab = repoPc.filtrar("rog")
                .map(c -> c.getCpu())//Del ordenador que devuelve el filtrar obtenemos su cpu
                .map(f -> f.getFabricante())//De la cpu que hemos obtenido previamente obtenemos su fabricante
                .filter(f -> f.getNombre().equalsIgnoreCase("intel")) //Al obtener el fabricante vamos a filtrar solo por aquellos que sean Intel
                .map(f -> f.getNombre()) //Una vez tenemos el fabricante obtenemos su nombre
                .orElseThrow(); //Si no se ha encontrado nada (Tambien podriamos utilizar el orElse())

        System.out.println(fab);
    }
}