package org.optional.ejemplo.repositorio;

import org.optional.ejemplo.models.Ordenador;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface Repositorio<T>{
    Optional<Ordenador> filtrar(String nombre);
}
