package org.optional.ejemplo.repositorio;

import org.optional.ejemplo.models.Fabricante;
import org.optional.ejemplo.models.Ordenador;
import org.optional.ejemplo.models.Procesador;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class OrdenadorRep implements Repositorio<Ordenador> {
    private List<Ordenador> dataSource;

    public OrdenadorRep() {
        dataSource = new ArrayList<>();

        Procesador proc = new Procesador("I9-11900H", new Fabricante("Intel"));
        Ordenador msi = new Ordenador("MSI Creator Z16", "Creator Z16A20");
        msi.setCpu(proc);
        dataSource.add(msi);
        dataSource.add(new Ordenador("Asus ROG", "Strix G512"));
    }

    @Override
    public Optional<Ordenador> filtrar(String nombre) {
        return dataSource.stream().filter(c -> c.getNombre().toLowerCase().contains(nombre.toLowerCase())).findFirst();
//        for(Ordenador or: dataSource){
//            if(or.getNombre().equalsIgnoreCase(nombre)){
//                return Optional.of(or); //Si encontramos el ordenador lo devolvemos
//            }
//        }
//        return Optional.empty(); //Si no encontramos el ordenador devolvemos un Optional vacio
    }
}
