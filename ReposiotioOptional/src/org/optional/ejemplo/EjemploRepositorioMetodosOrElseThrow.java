package org.optional.ejemplo;

import org.optional.ejemplo.models.Ordenador;
import org.optional.ejemplo.repositorio.OrdenadorRep;
import org.optional.ejemplo.repositorio.Repositorio;

import java.util.Optional;

public class EjemploRepositorioMetodosOrElseThrow {
    public static void main(String[] args) {
        Repositorio<Ordenador> repoPc = new OrdenadorRep(); //Obtenemos un nuevo ordenador a traves del repo

        Ordenador pc = repoPc.filtrar("MSI").orElseThrow(() -> new IllegalStateException()); //Obtenemos del repositorio el ordenador que tenga MSI o si no hay ninguno el que hemos creado por defecto
        // Ordenador pc = repoPc.filtrar("MSIfd").orElseThrow(IllegalStateException::new);

        System.out.println(pc);

        String archivo = "documento.pdf";
        String extension = Optional.ofNullable(archivo)
                .filter(a -> a.contains("."))
                .map(a -> a.substring(archivo.lastIndexOf(".") + 1))
                .orElseThrow();

        System.out.println(extension);
    }
}
