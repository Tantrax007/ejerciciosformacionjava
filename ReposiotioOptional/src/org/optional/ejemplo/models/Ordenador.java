package org.optional.ejemplo.models;

public class Ordenador {
    private String nombre, modelo;
    private Procesador cpu;

    public Ordenador(String nombre, String modelo) {
        this.nombre = nombre;
        this.modelo = modelo;
    }

    public Procesador getCpu() {
        return cpu;
    }

    public void setCpu(Procesador cpu) {
        this.cpu = cpu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Override
    public String toString() {
        return "nombre='" + nombre + '\'' +
                ", modelo='" + modelo + '\'';
    }
}
