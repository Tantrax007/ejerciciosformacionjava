package org.optional.ejemplo;

import org.optional.ejemplo.models.Ordenador;
import org.optional.ejemplo.repositorio.OrdenadorRep;
import org.optional.ejemplo.repositorio.Repositorio;

public class EjemploRepositorioMetodosOrElse {
    public static void main(String[] args) {
        Repositorio<Ordenador> repoPc = new OrdenadorRep(); //Obtenemos un nuevo ordenador a traves del repo
        Ordenador byDefault = new Ordenador("Ordenador Por", "Defecto"); //Vamos a crear un ordenador que se imprima por defecto

        Ordenador pc = repoPc.filtrar("MSI").orElse(byDefault); //Obtenemos del repositorio el ordenador que tenga MSI o si no hay ninguno el que hemos creado por defecto
        System.out.println(pc);

        pc = repoPc.filtrar("ROG").orElseGet(() -> new Ordenador("Ordenador por Defecto", "")); //Lo mismo, obtenemos el ordenador que sea ROG pero en este caso utilizamos orElseGet() para
        // devolver un objeto por defecto.

        /*
        Diferencias entre orElse y orElseGet
            orElse:
                Este instancia un objeto se encuentre o no el que queremos previamente, es decir, instancia un objeto que puede ser innecesario

            orElseGet:
                No crea ninguna instancia cuando el objeto que queremos se encuentra, por ende mejora en recursos
         */
    }
}
