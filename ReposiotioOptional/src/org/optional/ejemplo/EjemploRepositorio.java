package org.optional.ejemplo;

import org.optional.ejemplo.models.Ordenador;
import org.optional.ejemplo.repositorio.OrdenadorRep;
import org.optional.ejemplo.repositorio.Repositorio;

import java.util.Optional;

public class EjemploRepositorio {
    public static void main(String[] args) {

        Repositorio<Ordenador> repositorio = new OrdenadorRep();

        repositorio.filtrar("MSI").ifPresentOrElse(System.out::println, () -> System.out.println("No se ha encontrado el ordenador"));

//        Optional<Ordenador> pc = repositorio.filtrar("MSI");

//        if(pc.isPresent()) { //Si el opcional que se devuelve contiene algo
//            System.out.println(pc.get().getNombre());
//        }
//        else { //Si el opcional esta vacio
//            System.out.println("No se ha encontrado el modelo");
//        }
    }
}
