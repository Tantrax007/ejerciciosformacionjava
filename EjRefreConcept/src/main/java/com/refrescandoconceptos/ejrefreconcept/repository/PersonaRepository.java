package com.refrescandoconceptos.ejrefreconcept.repository;

import com.refrescandoconceptos.ejrefreconcept.model.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {
    //No implementa nada, solamente extiende para darnos la funcionalidad de la interfaz
}
