package com.refrescandoconceptos.ejrefreconcept.service;

import com.refrescandoconceptos.ejrefreconcept.dto.InpDTOPersona;
import com.refrescandoconceptos.ejrefreconcept.dto.OutDTOPersona;
import com.refrescandoconceptos.ejrefreconcept.model.Persona;
import com.refrescandoconceptos.ejrefreconcept.repository.PersonaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PersonaService implements IPersona{
    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public OutDTOPersona obtenerPersonaById(Long id) {
        Optional<Persona> respuesta = personaRepository.findById(id);

        if(respuesta.isPresent()){
            Persona per = respuesta.get();
            OutDTOPersona outPersona = new OutDTOPersona();
            outPersona.setId(per.getId());
            outPersona.setUsuario(per.getUsuario());
            outPersona.setName(per.getName());
            outPersona.setSurname(per.getSurname());
            return outPersona;
        }
        else{
            log.error("La persona con id " + id + " no ha sido encontrada en la BBDD");
            return null;
        }
    }

    @Override
    public List<OutDTOPersona> obtenerTodasPersonas() {
        List<OutDTOPersona> personasEncontradas = new ArrayList<>();

        personaRepository.findAll().forEach(per -> {
            OutDTOPersona outPersona = new OutDTOPersona();
            outPersona.setId(per.getId());
            outPersona.setUsuario(per.getUsuario());
            outPersona.setName(per.getName());
            outPersona.setSurname(per.getSurname());
            personasEncontradas.add(outPersona);
        });

        return personasEncontradas; //Si el listado esta vacio se manda vacio sin tener que controlarlo
    }

    @Override
    public void agregarPersona(InpDTOPersona persona) {
        Persona nuevaPersona = new Persona();
        nuevaPersona.setPassword(persona.getPassword());
        nuevaPersona.setName(persona.getName());
        nuevaPersona.setUsuario(persona.getUsuario());
        nuevaPersona.setSurname(persona.getSurname());
        personaRepository.save(nuevaPersona);
    }

    @Override
    public Boolean actualizarPersonaById(long id, InpDTOPersona persona) {
        if(personaRepository.findById(id).isPresent()){
            Persona actualizarPersona = new Persona();
            actualizarPersona.setId(id);
            actualizarPersona.setUsuario(persona.getUsuario());
            actualizarPersona.setName(persona.getName());
            actualizarPersona.setPassword(persona.getPassword());
            actualizarPersona.setSurname(persona.getSurname());

            personaRepository.save(actualizarPersona);
            return true;
        }else {
            log.error("La persona con id " + id + " no ha sido encontrada en la BBDD");
            return false;
        }
    }

    @Override
    public Boolean eliminarPersonaById(Long id) {
        if(personaRepository.findById(id).isPresent()){
            personaRepository.deleteById(id);
            return true;
        }else{
            log.error("La persona con id " + id + " no ha sido encontrada en la BBDD");
            return false;
        }
    }
}
