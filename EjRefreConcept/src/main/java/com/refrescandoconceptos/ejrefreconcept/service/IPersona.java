package com.refrescandoconceptos.ejrefreconcept.service;

import com.refrescandoconceptos.ejrefreconcept.dto.InpDTOPersona;
import com.refrescandoconceptos.ejrefreconcept.dto.OutDTOPersona;
import com.refrescandoconceptos.ejrefreconcept.model.Persona;
import org.springframework.stereotype.Component;

import java.util.List;

public interface IPersona {
    OutDTOPersona obtenerPersonaById(Long id);
    List<OutDTOPersona> obtenerTodasPersonas();
    void agregarPersona(InpDTOPersona persona);
    Boolean actualizarPersonaById(long id, InpDTOPersona persona);
    Boolean eliminarPersonaById(Long id);
}
