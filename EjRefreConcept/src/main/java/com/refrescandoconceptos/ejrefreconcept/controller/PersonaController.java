package com.refrescandoconceptos.ejrefreconcept.controller;

import com.refrescandoconceptos.ejrefreconcept.dto.InpDTOPersona;
import com.refrescandoconceptos.ejrefreconcept.dto.OutDTOPersona;
import com.refrescandoconceptos.ejrefreconcept.service.IPersona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/persona")
public class PersonaController {
    @Autowired
    private IPersona personaService;

    @GetMapping("/obtenerPersona/{id}")
    public OutDTOPersona obtenerHola(@PathVariable(value="id") long idPersona){
        return personaService.obtenerPersonaById(idPersona);
    }

    @GetMapping("/obtenerTodasPersonas")
    public List<OutDTOPersona> obtenerTodasPersonas(){
        return personaService.obtenerTodasPersonas();
    }

    @PostMapping("/agregarPersona")
    public boolean agregarHola(@Valid @RequestBody InpDTOPersona nuevaPersona){
        personaService.agregarPersona(nuevaPersona);
        return true;
    }

    @PutMapping("/actualizarPersona/{id}")
    public boolean actualizarPersona(@PathVariable(value="id") long idPersona, @Valid @RequestBody InpDTOPersona actPersona){
        return personaService.actualizarPersonaById(idPersona, actPersona);
    }

    @DeleteMapping("/eliminarPersona/{id}")
    public boolean deletePersona(@PathVariable(value="id") long idPersona){
        return personaService.eliminarPersonaById(idPersona);
    }
}
