package com.refrescandoconceptos.ejrefreconcept.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Lo genera la BBDD
    private Long id;

    private String usuario;
    private String password;
    private String name;
    private String surname;
}
