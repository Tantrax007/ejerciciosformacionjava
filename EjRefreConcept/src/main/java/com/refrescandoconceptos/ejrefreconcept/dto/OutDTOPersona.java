package com.refrescandoconceptos.ejrefreconcept.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class OutDTOPersona {
    private Long id;
    private String usuario;
    private String name;
    private String surname;
}
