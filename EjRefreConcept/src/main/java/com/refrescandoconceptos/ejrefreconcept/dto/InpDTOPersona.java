package com.refrescandoconceptos.ejrefreconcept.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@Component
public class InpDTOPersona {
    @NotNull(message="El username no puede ser nulo")
    private String usuario;
    @NotNull(message="La contraseña no puede ser nula")
    private String password;
    @NotNull(message="El nombre no puede ser nulo")
    private String name;
    @NotNull(message="El surname no puede ser nulo")
    private String surname;
}
