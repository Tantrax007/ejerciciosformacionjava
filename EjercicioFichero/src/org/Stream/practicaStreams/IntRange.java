package org.Stream.practicaStreams;

import java.util.stream.IntStream;

public class IntRange {
    public static void main(String[] args) {
        IntStream rango = IntStream
                .range(0, 100) //Cogemos del 0 al 19
                .peek(System.out::println);

        int suma = rango.sum(); //Sumamos todos los elementos del rango
        System.out.println(suma);

        IntStream rangoCerrado = IntStream
                .rangeClosed(0, 100) //Cogemos del 0 al 20
                .peek(System.out::println);

        int sumaCerrada = rangoCerrado.sum(); //Sumamos todos los elementos del rango
        System.out.println(sumaCerrada);
    }
}
