package org.Stream.practicaStreams;

import java.util.Optional;
import java.util.stream.Stream;

public class StreamsAnyMatch {
    public static void main(String[] args) {
        boolean nombres = Stream //No es un Stream porque directamente devuelve un booleano en funcion de si alguno cumple la condicion o no
                .of("Alberto Garzon", "Javier Sanchez", "Gabriel Gonzalez", "Jhon Doe", "Cristina Zuh", "Alberto Garcia", "Alberto Bazan")
                .anyMatch(match -> match.split("\u0020")[0].equals("Alberto")); //Esta condicion si la cumple al menos uno
//                .anyMatch(match -> match.split("\u0020")[0].equals("Nahuel")); Esta condicion no la cumple ninguno

        System.out.println(nombres);
    }
}
