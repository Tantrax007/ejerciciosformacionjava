package org.Stream.practicaStreams;

import models.Usuario;

import java.util.stream.Stream;

public class flatMap {
    public static void main(String[] args) {
        Stream<Usuario> Flatmap = Stream
                .of("Pato Guzman", "Paco Gonzalez", "Pepa Gutierrez", "Pepe Mena", "Pepe Garcia")
                .map(nombre -> new Usuario(nombre.split("\u0020")[0], nombre.split("\u0020")[1]))
                .flatMap(u -> {
                    if(u.getNombre().equalsIgnoreCase("Pepe")){
                        return Stream.of(u); //Si se cumple el flat map devuelve un Stream con el objeto
                    }
                    return Stream.empty(); //Si no devuelve un Stream vacio
                });

        Flatmap.forEach(System.out::println);
    }
}
