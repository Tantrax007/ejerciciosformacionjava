package org.Stream.practicaStreams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsFilter {
    public static void main(String[] args) {
        Stream<String> nombres = Stream
                .of("Alberto Garzon", "Javier Sanchez", "Gabriel Gonzalez", "Jhon Doe", "Cristina Zuh", "Alberto Garcia", "Alberto Bazan")
                .filter(nombre -> nombre.split("\u0020")[0].equals("Alberto")); //Filtramos por aquellos cuyo nombre sea Alberto

        List<String> nombreFiltrado = nombres.collect(Collectors.toList()); //Convertimos el filtrado
        nombreFiltrado.forEach(System.out::println);
    }
}
