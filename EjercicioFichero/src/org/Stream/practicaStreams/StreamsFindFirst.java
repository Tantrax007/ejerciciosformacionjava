package org.Stream.practicaStreams;

import java.util.Optional;
import java.util.stream.Stream;

public class StreamsFindFirst {
    public static void main(String[] args) {
        String nombres = Stream //No es un Stream porque directamente devuelve un String
                .of("Alberto Garzon", "Javier Sanchez", "Gabriel Gonzalez", "Jhon Doe", "Cristina Zuh", "Alberto Garcia", "Alberto Bazan")
                .filter(nombre -> nombre.split("\u0020")[0].equals("Alberto"))
                .findFirst().get();

        System.out.println(nombres);


//        Otra forma de hacerlo
        Stream<String> nombres2 = Stream //No es un Stream porque directamente devuelve un String
                .of("Alberto Garzon", "Javier Sanchez", "Gabriel Gonzalez", "Jhon Doe", "Cristina Zuh", "Alberto Garcia", "Alberto Bazan")
                .filter(nombre -> nombre.split("\u0020")[0].equals("Alberto"));

        Optional<String> albertos = nombres2.findFirst();
        System.out.println(albertos.get()); //Obtenemos el String que se ha almacenado en el optional
    }
}
