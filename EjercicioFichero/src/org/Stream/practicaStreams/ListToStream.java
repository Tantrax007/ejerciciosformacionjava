package org.Stream.practicaStreams;

import models.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ListToStream {
    public static void main(String[] args) {
        List<Usuario> lista = new ArrayList<Usuario>();
        lista.add(new Usuario("Andres", "Mendoza"));
        lista.add(new Usuario("Alberto", "Garzon"));
        lista.add(new Usuario("Nahuel", "Omar"));
        lista.add(new Usuario("Lola", "Flores"));
        lista.add(new Usuario("Pepe", "Fernandez"));
        lista.add(new Usuario("Bruce", "Lie"));
        lista.add(new Usuario("Jaime", "Lorenzo"));

        Stream<String> nombres = lista.stream()
                .map(u -> u.getNombre() + " " + u.getApellido())
                .flatMap(nombre -> {
                    if(nombre.contains("Bruce")){
                        return Stream.of(nombre);
                    }
                    return Stream.empty();
                })
                .peek(System.out::println);

        System.out.println(nombres.count());
    }
}
