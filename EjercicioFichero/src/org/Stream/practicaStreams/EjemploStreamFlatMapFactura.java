package org.Stream.practicaStreams;

import models.Factura;
import models.Usuario;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EjemploStreamFlatMapFactura {
    public static void main(String[] args) {
        Usuario user1 = new Usuario("Jhone", "Doe");
        Usuario user2 = new Usuario("Pepe", "Perez");

        user1.addFactura(new Factura("Compras tecnologia"));
        user1.addFactura(new Factura("Compras muebles"));

        user2.addFactura(new Factura("Bicicleta"));
        user2.addFactura(new Factura("Notebook Gamer"));

        List<Usuario> usuarios = Arrays.asList(user1, user2);
        usuarios.stream().flatMap(usuario -> usuario.getFacturas().stream())
                .forEach(f -> System.out.println(f.getDescripcion()));
    }
}
