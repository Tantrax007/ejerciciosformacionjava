package org.Stream.practicaStreams;

import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;

public class Statistics {
    public static void main(String[] args) {
        IntStream rango510 = IntStream.range(5, 20).peek(System.out::println);

//    int resultado = rango510.reduce(0, Integer::sum);
//    int resultado = rango510.sum();

        IntSummaryStatistics stats = rango510.summaryStatistics(); // Esta nos da una serie de metodos por ejemplo para sacar el maximo, o el minimo, o la suma...
        System.out.println("Max: " + stats.getMax());
        System.out.println("Min: " + stats.getMin());
        System.out.println("Promedio: " + stats.getAverage());
    }
}
