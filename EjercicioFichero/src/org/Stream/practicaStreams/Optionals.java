package org.Stream.practicaStreams;

import java.util.Optional;
import java.util.stream.Stream;

public class Optionals {
    public static void main(String[] args) {
        Stream<String> nombres2 = Stream //No es un Stream porque directamente devuelve un String
                .of("Alberto Garzon", "Javier Sanchez", "Gabriel Gonzalez", "Jhon Doe", "Cristina Zuh", "Alberto Garcia", "Alberto Bazan")
                .filter(nombre -> nombre.split("\u0020")[0].equals("Alberto"));

        Optional<String> albertos = nombres2.findFirst();
        System.out.println(albertos.get()); //Obtenemos el String que se ha almacenado en el optional
        System.out.println(albertos.orElse("Null")); //Si no se encuentra nada en el Optional lanzamos el string "Null"
        System.out.println(albertos.orElseThrow()); //Si no se encuentra nada en el Optional lanzamos la excepcion "NoSuchElementException"
    }
}
