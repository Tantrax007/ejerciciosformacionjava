package com.bosonit.formacion.ejerciciospring.ejerciciospringboot.controllers;

import com.bosonit.formacion.ejerciciospring.ejerciciospringboot.models.Persona;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class index {

    //Metodo para obtener un saludo en funcion del nombre dado en el PathVariable
    @GetMapping(value="/user/{nombre}")
    public String getUser(@PathVariable(value="nombre") String nombre, Model model){
        model.addAttribute("titulo", "Obtener usuario " + nombre);
        model.addAttribute("saludo", "Hola " + nombre);
        return "user";
    }

    @PostMapping("/useradd")
    public @ResponseBody Persona crearPersona(@RequestBody Persona persona) { //Con el @ResponseBody indicamos a SpringBoot que lo que vamos a coger es un JSON
        Persona respuesta = new Persona(); //Creamos un objeto persona
        int edadPersona = Integer.parseInt(persona.getEdad()) + 1; //Obtenemos del campo JSON "edad" con el getter simplemente
        respuesta.setEdad(edadPersona + "");
        respuesta.setNombre(persona.getNombre());//Obtenemos del campo JSON "nombre" con el getter simplemente
        respuesta.setPoblacion(persona.getPoblacion());//Obtenemos del campo JSON "poblacion" con el getter simplemente
        return respuesta; //Devolvemos el objeto persona que se muestra como JSON
        // OJO PORQUE SI LOS CAMPOS DEL JSON NO SE LLAMAN IGUAL QUE LOS GETTERS NO VA A PILLAR EL VALOR
    }
}
