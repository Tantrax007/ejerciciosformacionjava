package com.bosonit.formacion.demojpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

@RestController
@RequestMapping(value="/usuario")
public class ControladorUsuario {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(value="/obtener/{id}")
    public Usuario getUser(@PathVariable String id) throws FileNotFoundException {
        return usuarioService.obtenerUsuario(id);
    }

    @PostMapping(value="/insertar")
    public void insertUser(@RequestBody Usuario usuario){
        usuarioService.crearUsuario(usuario);
    }

    @PutMapping(value="/actualizar/{id}")
    public void updateUser(@RequestBody Usuario usuario, @PathVariable String id){
        actualizarUsuario.accept(usuario, id);
    }

    BiConsumer<Usuario, String> actualizarUsuario = (user, identificador) -> {
        try{
            usuarioService.obtenerUsuario(identificador); //Si el usuario no existe se lanza excepcion
            usuarioService.actualizarUsuario(user); //Si existe usuario se actualiza
        }
        catch (FileNotFoundException userNotFound){
            System.out.println("No se ha encontrado usuario con este id: " + identificador);
            System.out.println("No se ha podido actualizar el usuario");
        }
    };

    @DeleteMapping(value="/eliminar/{id}")
    public void deleteUser(@PathVariable String id){
        usuarioService.eliminarUsuario(id);
    }
}