package com.bosonit.formacion.demojpa;

import java.io.FileNotFoundException;

public interface UsuarioService {
    void crearUsuario(Usuario usuario);
    Usuario actualizarUsuario(Usuario usuario);
    Usuario obtenerUsuario(String id) throws FileNotFoundException;
    void eliminarUsuario(String id);
}
