package com.bosonit.formacion.demojpa;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Data
@Entity
@Table(name="usuario")
public class Usuario implements java.io.Serializable {
    @Id
    private String id;

    private String username;
    private int edad;
}
