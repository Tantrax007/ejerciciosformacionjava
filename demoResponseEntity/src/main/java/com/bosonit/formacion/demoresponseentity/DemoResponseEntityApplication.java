package com.bosonit.formacion.demoresponseentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoResponseEntityApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoResponseEntityApplication.class, args);
    }

}
