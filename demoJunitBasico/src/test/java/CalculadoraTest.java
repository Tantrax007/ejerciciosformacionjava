import org.hamcrest.Matcher;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class CalculadoraTest {

    @Test
    @Disabled
    void suma() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.suma(32, 94);
        assertThat(resultado, is(107)); //Valor que deberia dar, Variable a comparar
    }

    @Test
    @DisplayName("Método de Resta")
    void resta() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.resta(97, 23);
        assertEquals(74, resultado); //Valor que deberia dar, Variable a comparar
    }

    @Test
    @Tag("avanzada")
    void division() {
        Calculadora calculadora = new Calculadora();
        double resultado = calculadora.division(20, 5);
        assertEquals(4, resultado); //Valor que deberia dar, Variable a comparar
    }

    @Test
    @Tag("avanzada")
    void multiplicacion() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.multiplicacion(100, 6);
        assertEquals(600, resultado); //Valor que deberia dar, Variable a comparar
    }

    @Test
    void suma2() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.suma(32, 94);
        assertThat(resultado, is(126)); //Valor que deberia dar, Variable a comparar
    }
}