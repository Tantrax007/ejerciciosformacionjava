package com.bosonit.formacion.jpa.ejerciciojpa.controller;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.service.IPersona;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/persona")
public class IndexController {

    @Autowired
    private IPersona iPersona;

    @GetMapping(value="/getPersona/porId/{id}")
    public OutPersonaDTO getPersona(@PathVariable int id) throws Exception{
        return iPersona.getPersonaDTO(id);
    }

    @GetMapping(value="/getPersona/all")
    public List<OutPersonaDTO> getPersonas() {
        return iPersona.getAllPersonas();
    }

    @PostMapping(value="/setPersona")
    public void setPersona(@RequestBody InpPersonaDTO personaEntrada) throws Exception {
        iPersona.setPersonaDTO(personaEntrada);
    }

//    @DeleteMapping(value="/deletePersona/{id}")
//    public void deletePersona(@PathVariable int id) throws Exception{
//        iPersona.deletePersonaById(id);
//    }
}
