package com.bosonit.formacion.jpa.ejerciciojpa.service;

import com.bosonit.formacion.jpa.ejerciciojpa.dto.InpPersonaDTO;
import com.bosonit.formacion.jpa.ejerciciojpa.dto.OutPersonaDTO;

import java.util.List;

public interface IPersona {
    public void setPersonaDTO(InpPersonaDTO personaEntrada) throws Exception;
    public OutPersonaDTO getPersonaDTO(int id) throws Exception;
    public List<OutPersonaDTO> getAllPersonas();
//    public boolean deletePersonaById(int id) throws Exception;
}
